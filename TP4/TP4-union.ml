(* TP 4  Fil et FAP *)
(* Alexandre Lithaud | Samuel Brun *)


(* ======================== 4 - File et file à priorités ======================== *)

(* file : soit Vide, soit une construction entre un élément et une autre file
   Nous avons fait le choix de ne pas utiliser les listes natives pour bien différencier les cas d'utilisation *)
   
type 'a file =
  | Nil
  | Cons of 'a*'a file;;

exception FileVide;;
exception File2Vide;;
exception FapVide;;

(* Deux files *)
let (f1:int file) = Cons(5,Cons(3,Nil));;
let (f2:int file) = Cons(5,Cons(3,Cons(4,Cons(9,Nil))));;

(* ============ 4.1 Structure de file ============ *)

(* Exo 4.1 *)
(* La file vide est la file Nil *)
let (file_vide:'a file) = Nil;;

(*
est_file_vide
Entrée : f une 'a file
Sortie : Un  booleen
Spec : true si la file est vide, false sinon
*)
let (est_file_vide:'a file->bool) = fun f -> (f = Nil);;
(*Tests*)
assert(est_file_vide file_vide = true);;
assert(est_file_vide f1 = false);;

(*
enfile
Entrée : e un 'a élément, f une 'a file
Sortie : Une 'a file
Spec : enfile l'element en tete de file
*)
let (enfile:'a->'a file->'a file) = fun e f -> Cons(e,f);;
(*Tests*)
enfile 1 f1;;
enfile 2 f2;;
enfile 1 file_vide;;

(*
defile
Entrée : f une 'a file
Sortie : un couple d'un 'a et d'une 'a file
Spec : récupere l'élément en bout de file et donne un couple composé de l'element et de la file initiale privée
  de ce dernier. Leve un exception en cas de file vide
*)
let rec (defile:'a file->('a*'a file)) = fun f ->
  match f with
  | Nil -> raise FileVide
  | Cons (e,Nil) -> (e,Nil)
  | Cons (e,s) -> let (a,b) = defile(s) in (a,Cons(e,b));;
(*Tests*)
defile f1;;
defile f2;;
defile file_vide;; (* On verifie la génération d'exception *)



(* Exo 4.2 *)

let rec (to_list_rec:('a file)->('a list)->('a list)) = fun f l->
  if(est_file_vide f) then l else (let (e,s) = defile f in to_list_rec s (e::l));;
(*
to_list
Entrée : f une 'a file
Sortie : une liste native ocaml
Spec : construit une liste a partir des éléments contenus dans la file par défilement succesifs.
*)
let (to_list:('a file)->('a list)) = fun f -> 
  to_list_rec f [];;
(*Tests*)
to_list f1;;
to_list f2;;
to_list file_vide;;


let rec (to_file_rec:('a list)->('a file)->('a file)) = fun l f->
  match l with
  | [] -> f
  | e :: s -> to_file_rec s (enfile e f);;
(*
to_file
Entrée : l une liste native d'ocaml
Sortie : une 'a file
Spec : construit une 'a file a partir de la liste en enfilant les éléments successifs
*)
let (to_file:('a list)->('a file)) = fun l -> 
  to_file_rec l file_vide;;
(*Tests*)
to_file [1;2;3;4;5];;
to_file [];;
to_file [1];;

(*
revertList
Entrée : l,acc deux listes natives ocaml
Sortie : une liste native ocaml
Spec : renverse la premiere liste en se servant de la 2eme comme accumulateur
*)
let rec (revertList:'a list->'a list->'a list) = fun l acc->
  match l with
  | [] -> acc
  | e::s -> revertList s (e::acc);;
(*Tests*)
revertList [1;2;3;4;5] [];;
revertList [] [];;
revertList [1] [];;

(*
equal
Entrée : l,acc deux listes natives ocaml
Sortie : un bool
Spec : true si les 2 listes sont identiques, false sinon
*)
let rec (equal:'a list->'a list->bool) = fun l1 l2 ->
  match l1,l2 with
  | [],[] -> true
  | x::s,x'::s' -> (x=x') && equal s s'
  | _ -> false;;
(*Tests*)
assert(equal [1] [1]);;
assert(equal [] [1] = false);;
assert(equal [] []);;
assert(equal [2] [1] = false);;
assert(equal [4;2;1] [1;2;4] = false);;
assert(equal [1;2;4] [1;2;4]);;

(*
teste_file
Entrée : l une liste native ocaml
Sortie : un bool
Spec : true si la combinaison to_list( to_file (l)) = renversement de l
*)
let (teste_file:'a list -> bool) = fun l ->
  equal (to_list (to_file l)) (revertList l []);;
(*Tests*)
assert (teste_file []);;
assert (teste_file [1;3;4;0;9;8]);;
assert (teste_file [1]);;




(* ============= 4.1 File (Version 2) ============ *)

(* Exo 4.3 *)

(*file2 : est une construction entre 2 listes ; Une d'entrée et une de sortie  
Nous avons fait le choix de ne pas utiliser les listes natives pour bien différencier les cas d'utilisation *)

type 'a file2 = ('a list) * ('a list);;

let (file_vide2:'a list*'a list) = [],[];;

(*
est_file_vide2
Entrée : f une 'a file2
Sortie : Un  booleen
Spec : true si la file est vide, false sinon
*)
let (est_file_vide2:'a file2->bool) = fun f -> let l1,l2 = f in 
  match l1,l2 with
  | [],[] -> true
  | _,_ -> false;;
(*Tests*)
assert(est_file_vide2 file_vide2 = true);;
assert(est_file_vide2 ([1;2],[]) = false);;
assert(est_file_vide2 ([1;2],[1]) = false);;

(*
enfile2
Entrée : e un 'a élément, f une 'a file2
Sortie : Une 'a file2
Spec : enfile l'element dans la premiere liste de notre file
*)
let (enfile2:'a->'a file2->'a file2) = fun e (l1,l2) -> (e::l1,l2);;
(*Tests*)
enfile2 1 ([2;3],[8;2;9]);;
enfile2 2 ([],[8;2;9]);;
enfile2 1 file_vide2;;

(*
defile2
Entrée : f une 'a file2
Sortie : un couple d'un 'a et d'une 'a file2
Spec : si la seconde liste est non vide, on prends simplement l'element en tete, sinon on renverse la premiere liste 
  dans la seconde et on défile a partir du nouveau résultat
*)
let rec (defile2:'a file2->'a*'a file2) = fun (l1,l2) ->
  match l1,l2 with
  | [],[] -> raise File2Vide
  | [],x::s -> (x,([],s))
  | x::s, [] -> defile2 ([],revertList (x::s) [])
  | x::s,x'::s' ->(x',(x::s,s'));;
(*Tests*)
defile2 ([1;2],[3]);;
defile2 ([1;2],[3;8;9]);;
defile2 ([],[3]);;
defile2 ([1;2],[]);;
defile2 ([1;2;9;7],[]);;
defile2 file_vide2;; (* On verifie la génération d'exception *)


(* ============= 4.2 File à Priorités ============ *)

(* Exo 4.4 *)

(*fap : soie Vide, soie une construction entre un element, une priorité de type int et une autre fap 
Nous avons fait le choix de ne pas utiliser les listes natives pour bien différencier les cas d'utilisation *)

type 'a fap =
  | Nil
  | Cons of 'a*int*'a fap;;

let (fap_vide:'a fap) = Nil;;
let (f:int fap) = Cons(1,5,Cons(2,4,Nil));;

(*
est_fap_vide :
Entrée : Une file a priorité de 'a
Sortie : Un booleen
Spec : Fonction qui indique si la file a priorité est nulle ou non
*)
let (est_fap_vide:'a fap->bool) = fun f -> (f = Nil);;
(*Tests*)
assert(est_fap_vide fap_vide = true);;
assert(est_fap_vide f = false);;

(*
insere :
Entrée : Un element de 'a, un priorité int, une file a priorité de 'a
Sortie : Une file a priorité de 'a
Spec : Fonction qui insere le nouvelle element avec la nouvelle priorité au bonne endroit de la fap
*)
let rec (insere:'a->int->'a fap->'a fap) = fun e p f -> 
  match f with
  | Nil -> Cons(e,p,f)
  | Cons(e',p',f') -> if (p' < p) then Cons(e,p,Cons(e',p',f')) else Cons(e',p',insere e p f');;
(*Tests*)
insere 3 5 f;;
insere 3 0 f;;
insere 3 10 f;;
insere 3 0 fap_vide;;

(*
extrait :
Entrée : Une file a priorité de 'a
Sortie : Un couple d'un element de 'a et une file a priorité de 'a
Spec : Fonction extraiyant l'element de la file a priorité ayant la priorité la plus adequat
*)
let rec (extrait:'a fap->('a * 'a fap)) = fun f ->
  match f with
  | Nil -> raise FapVide
  | Cons(e,p,f') -> (e,f');;
(*Tests*)
extrait f;;
extrait fap_vide;;

(* Exo 4.5 *)

let rec (to_fap_rec:'a list->int->'a fap) = fun l n->
  match l with
  | [] -> fap_vide
  | x::s -> insere x (n) (to_fap_rec s (n+1));;

(*
to_fap :
Entrée : Une liste de 'a
Sortie : Une file a priorité de 'a
Spec : Fonction créant une file a priorité a partir d'une liste
*)
let rec (to_fap:'a list->'a fap) = fun l ->
  to_fap_rec l 0;;
(*Tests*)
to_fap [];;
to_fap [1;3;0;4];;
to_fap [1];;

(*
to_list_fap :
Entrée : Une file a priorité de 'a
Sortie : Une liste de 'a
Spec : Fonction créant une liste a partir d'une file
*)
let rec (to_list_fap:'a fap->'a list) = fun f ->
  if(est_fap_vide f) then [] else (let a,b = extrait f in a::(to_list_fap b));;

(*Tests*)
to_list_fap f;;
to_list_fap fap_vide;;

(*
teste_fap :
Entrée : Une liste de type 'a
Sortie : Un  booleen
Spec : Test de fonctionnement d'une fap utilisant des fonction exterieur
*)
let (teste_fap:'a list ->bool) = fun l ->
  equal (to_list_fap (to_fap l)) (revertList l []);;
(*Tests*)
assert(teste_fap [1;2;3;4]);;
assert(teste_fap [8;2;5;4]);;
assert(teste_fap [4;3;2]);;
assert(teste_fap [1]);;
assert(teste_fap []);;

(* ============================ FIN (obligatoire) ============================ *)

(* Exo 4.6 *)
(* Avec ABR *)
type 'a abr = 
  | F
  | N of ('a abr * ('a * int) * 'a abr);;

type 'a fap2 = 'a abr;;
let (f1:float fap2) = N(F,(1.,3),N(F,(2.,4),F));;

let (fap_vide2:'a fap2) = F;;
let (est_fap_vide2:'a fap2->bool) = fun f -> (f=F);;
assert (est_fap_vide2 fap_vide2);;
assert (est_fap_vide2 f1=false);;

let rec (insere2:'a->int->'a fap2->'a fap2) = fun e p f ->
  match f with
  | F -> N(F,(e,p),F)
  | N(a1,(e',p'),a2) -> if (p < p') then N((insere2 e p a1),(e',p'),a2) else N(a1,(e',p'),(insere2 e p a2));;

let rec (extraire2:'a fap2->('a*'a fap2)) = fun f ->
  match f with
  | F -> raise FapVide
  | N(F,(e',p'),F) -> (e',F)
  | N(F,(e',p'),a1) -> let a,b = extraire2 a1 in (a,N(F,(e',p'),b))
  | N(a1,(e',p'),F) -> (e',a1)
  | N(a1,(e',p'),a2) -> let a,b = extraire2 a2 in (a,N(a1,(e',p'),b));;

(* 1 élément *)
let f1 = insere2 3. 5 fap_vide2;;
let (a,b) = extraire2 f1;;

(* 2 éléments *)
let f1 = insere2 2. 5 (insere2 3. 4 fap_vide2);;
let (a,b) = extraire2 f1;;
let (c,d) = extraire2 b;;

let f1 = insere2 2. 4 (insere2 3. 5 fap_vide2);;
let (a,b) = extraire2 f1;;
let (c,d) = extraire2 b;;

(* 3 éléments *)
let f1 = insere2 1. 6 (insere2 2. 4 (insere2 3. 5 fap_vide2));;
let (a,b) = extraire2 f1;;
let (c,d) = extraire2 b;;
let (e,f) = extraire2 d;;

let f1 = insere2 1. 5 (insere2 2. 5 (insere2 3. 5 fap_vide2));;
let (a,b) = extraire2 f1;;
let (c,d) = extraire2 b;;
let (e,f) = extraire2 d;;

let f1 = insere2 1. 4 (insere2 2. 5 (insere2 3. 6 fap_vide2));;
let (a,b) = extraire2 f1;;
let (c,d) = extraire2 b;;
let (e,f) = extraire2 d;;

let f1 = insere2 1. 6 (insere2 2. 5 (insere2 3. 4 fap_vide2));;
let (a,b) = extraire2 f1;;
let (c,d) = extraire2 b;;
let (e,f) = extraire2 d;;

(* 3 éléments *)
let f1 = insere2 1. 0 (insere2 2. 8 (insere2 3. 6 (insere2 4. 4 (insere2 5. 5 fap_vide2))));;
let (a,b) = extraire2 f1;;
let (a,b) = extraire2 b;;
let (a,b) = extraire2 b;;
let (a,b) = extraire2 b;;
let (a,b) = extraire2 b;;

