(* TP 4  Fil et FAP *)
(* Alexandre Lithaud | Samuel Brun *)


(* ======================== 4 - File et file à priorités ======================== *)

type 'a file =
  | Nil
  | Cons of 'a * 'a file;;

exception FileVide;;

(* ============ 4.1 Structure de file ============ *)


(* Exo 4.1 *)

let (file_vide:'a file) = Nil;;

let (est_file_vide:'a file -> bool) = fun f ->
  match f with
    | Nil -> true;
    | Cons (e,s) -> false;;

let (enfile:'a->'a file->'a file) = fun e f -> Cons(e, f);;

let rec (defile:'a file->('a * 'a file)) = fun f ->
  match f with
    | Nil -> raise FileVide
    | Cons (e,Nil) -> (e, Nil)
    | Cons (e,s) -> let (a,b) = defile s in (a,Cons(e,b)) ;;

let f0 = Nil;;
let f1 = Cons(5,Cons(3,Nil));;
let f2 = Cons(5,Cons(3,Cons(4,Cons(9,Nil))));;
defile f1;;
defile f2;;
defile f0;;

(* Exo 4.2 *)

let rec list_to_file_rec = fun l f ->
  match l with
    | [] -> f
    | x::s -> let f2 = enfile x f in list_to_file_rec s f2;;

let (list_to_file:'a list->'a file) = fun l ->
  match l with
    | [] -> Nil
    | x::s -> list_to_file_rec l Nil;;


let testListe = [5;1;2;8;7;7;2;4];;
let _ = list_to_file testListe;;


let rec (file_to_list_rec:'a file->'a list->'a list) = fun f l ->
  match f with
    | Nil -> []
    | Cons (e,s) -> let(a,b) = defile f in a :: file_to_list_rec(b)(l) ;;

let (file_to_list:'a file->'a list) = fun f ->
  match f with
    | Nil -> []
    | Cons (e,s) -> file_to_list_rec f [];;

let _ = file_to_list f2;;


(*let rec (renv_app:'a list->'a list->'a list) = fun l1 l2 ->
  match l1 with
  | [] -> l2
  | x :: s -> renv_app s (x::l2);;

(*
renvV2 :
Entrée : une liste de 'a
Sortie : une liste de 'a
Spec : Fonction qui retourne les element de la liste en entrée
*)
let (renvV2:'a list->'a list) = fun l -> renv_app l [];;

renvV2 [];;
renvV2 [2;4;6;7];;*)


let rec teste_file_rec = fun l l_renv ->
  match l with
    | [] -> (match l_renv with
              | [] -> true
              | _::_ -> false)
    | x::s -> match l_renv with
                | [] -> false
                | x2::s2 -> if(x2 = x) then teste_file_rec(s)(s2) else false;;

let (teste_file:'a list->bool) = fun l ->
  match l with
    | [] -> raise FileVide
    | x::s -> teste_file_rec l (file_to_list (list_to_file l))


let testListe = [5;1;2;8;7;7;2;4];;
let _ = teste_file testListe;;

(* Exo 4.3 *)

type 'a file2 =
  | Nil
  | Cons of 'a list * 'a list;;

(* TODO FAIRE LES FONCTION DE BASE AVEC FILE2*)

(* ============= 4.2 File à Priorités ============ *)

type 'a fap =
  | Nil
  | Cons of int * 'a * 'a fap;;

exception FapVide;;

(* Exo 4.4 *)

let (fap_vide:'a fap) = Nil;;

let (est_fap_vide:'a fap -> bool) = fun fap ->
  match fap with
    | Nil -> true
    | Cons(p,e,s) -> false;;

let (insere:'a->int->'a fap->'a fap) = fun e p fap -> Cons(p,e,fap);;

let rec (extrait:'a fap->('a * 'a fap)) = fun fap ->
  match fap with
    | Nil -> raise(FapVide)
    | Cons(p,e,Nil) -> (e,Nil)
    | Cons(p,e,s) -> let (a,b) = extrait s in match b with
                                                  | Nil -> (a,Cons(p,e,b))
                                                  | Cons(p3,e3,s3) -> if(p > p3) then (e3,Cons(p3,e3,s3))  else (e,Cons(p,e,s3));;


let t = insere 5 5 (insere 9 1 (insere 4 7 (insere 10 6  (insere 4 10 fap_vide))));;
let _ =  extrait t;;

(* Exo 4.5 *)

(* Exo 4.6 *)

type 'a tas = ('a * int) array;;

let (tas_vide:'a tas) = Array.make 0 (0,0);;

let t = tas_vide;;
let _ = Array.length t;;

let est_tas_vide = fun t -> (Array.length t = 0);;


let rec insere_tas_req = fun t e p n ->
  if(est_tas_vide t) then Array.make 1 (e,p) else let(e2,p2) = t.get(n) in
        if(p > p2) then (*On s'arrete la*)  else (*On continue*) insere_tas_req t

let insere_tas = fun t e p ->
 insere_tas_req t e p 0;;
