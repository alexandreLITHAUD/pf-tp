type 'a file =
  | Nil
  | Cons of 'a*'a file;;

let f1 = Cons(5,Cons(3,Nil));;
let f2 = Cons(5,Cons(3,Cons(4,Cons(9,Nil))));;

let (file_vide:'a file) = Nil;;

let (est_file_vide:'a file->bool) = fun f ->
  match f with
  | Nil -> true
  | _ -> false;;

assert(est_file_vide file_vide = true);;
assert(est_file_vide f1 = false);;


let (enfile:'a->'a file->'a file) = fun e f -> Cons(e,f);;

enfile 1 f1;;
enfile 2 f2;;
enfile 1 file_vide;;


let rec (defile:'a file->('a*'a file)) = fun f ->
  match f with
  | Nil -> failwith("ERREUR : defile : file vide")
  | Cons (e,Nil) -> (e,Nil);
  | Cons (e,s) -> let (a,b) = defile(s) in (a,Cons(e,b));;

defile f1;;
defile f2;;

(* EXO 4.2 *)
let rec (to_list_rec:('a file)->('a list)->('a list)) = fun f l->
  if(est_file_vide f) then l else (let (e,s) = defile f in to_list_rec s (e::l));;

let (to_list:('a file)->('a list)) = fun f -> 
  to_list_rec f [];;

to_list f1;;
to_list f2;;
to_list file_vide;;


let rec (to_file_rec:('a list)->('a file)->('a file)) = fun l f->
  match l with
  | [] -> f
  | e :: s -> to_file_rec s (enfile e f);;

let (to_file:('a list)->('a file)) = fun l -> 
  to_file_rec l file_vide;;

to_file [1;2;3;4;5];;
to_file [];;
to_file [1];;

let rec (revertList:'a list->'a list->'a list) = fun l acc->
  match l with
  | [] -> acc
  | e::s -> revertList s (e::acc);;

let rec (equal:'a list->'a list->bool) = fun l1 l2 ->
  match l1,l2 with
  | [],[] -> true
  | x::s,x'::s' -> true
  | _ -> false;;

let (teste_file:'a list -> bool) = fun l ->
  equal (to_list (to_file l)) (revertList l []);;

assert (teste_file []);;
assert (teste_file [1;3;4;0;9;8]);;
assert (teste_file [1]);;

(* Exo 4.3 *)
type 'a file2 = ('a list) * ('a list);;
let (file_vide2:'a list*'a list) = [],[];;
let (est_file_vide2:'a file2->bool) = fun f -> let l1,l2 = f in 
  match l1,l2 with
  | [],[] -> true
  | _,_ -> false;;
let (enfile2:'a->'a file2->'a file2) = fun e (l1,l2) -> (e::l1,l2);;
let (defile2:'a file2->'a*'a file2) = fun (l1,l2) ->
  match l1,l2 with
  | [],[] -> failwith("ERREUR : defile2 : file vide")
  | [],x::s -> (x,([],s))
  | x::s, [] -> (x,([],revertList s []))
  | x::s,x'::s' ->(x',(x::s,s'));;

(* Tests fap *)

(* Exo 4.4 *)

type 'a fap =
  | Nil
  | Cons of 'a*int*'a fap;;

let (fap_vide:'a fap) = Nil;;
let (est_fap_vide:'a fap->bool) = fun f -> (f = Nil);;
let rec (insere:'a->int->'a fap->'a fap) = fun e p f -> 
  match f with
  | Nil -> Cons(e,p,f)
  | Cons(e',p',f') -> if (p' < p) then Cons(e,p,Cons(e',p',f')) else Cons(e',p',insere e p f');;
let rec (extrait:'a fap->('a * 'a fap)) = fun f ->
  match f with
  | Nil -> failwith("ERREUR : extraire : fap vide")
  | Cons(e,p,f') -> (e,f');;

(* Exo 4.5 *)
let rec (to_fap_rec:'a list->int->'a fap->'a fap) = fun l p f->
  match l with
  | [] -> f
  | x::s -> to_fap_rec s (p+1) (Cons(x,p,f));;
let (to_fap:'a list->'a fap) = fun l ->
  to_fap_rec l 0 fap_vide;;

let rec (to_list_fap_rec:'a fap->'a list->'a list) = fun f l->
 if(est_fap_vide f)then l else (let a,b = extrait f in to_list_fap_rec b (a::l));;
let (to_list_fap:'a fap->'a list) = fun f ->
  to_list_fap_rec f [];;

let (teste_fap:'a list ->bool) = fun l ->
  equal (to_list_fap (to_fap l)) (revertList l []);;

assert(teste_fap [1;2;3;4]);;
assert(teste_fap [8;2;5;4]);;
assert(teste_fap [4;3;2]);;
assert(teste_fap [1]);;
assert(teste_fap []);;

