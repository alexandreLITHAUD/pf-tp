(* TP 2 *)
(* AUTEURS : ALEXANDRE LITHAUD ; SAMUEL BRUN  *)


(* 2.1 *)

type abin =
  | Leaf
  | Node of int * abin * abin;;

let arbreVide = Leaf;;
let arbreAUnElem = Node(5,Leaf,Leaf);;
let arbreSimple = Node(5,Node(1,Node(2,Leaf,Node(10,Leaf,Leaf)),Leaf),Node(12,Leaf,Leaf));;

(* 
  countNode :
  Entrées : a un arbre binaire
  Sorties : un entier
  Spec : calcul recursivement le nombre de noeuds d'un abr, pour cela décomposons a. Si c'est une feuille,
    le nombre de noeuds est nulle, sinon, on compte 1 et on fait le calcul sur les sous arbres de gauche
    et droite.
*)
let rec (countNode:abin->int) = fun a ->
  match a with
    | Leaf -> 0
    | Node(i,a1,a2) -> 1 + countNode a1 + countNode a2;;

let testCountNode = countNode arbreVide;;
let _ = assert(countNode arbreVide = 0);;
let _ = assert(countNode arbreAUnElem = 1);;
let _ = assert(countNode arbreSimple = 5);;

(* 
  height :
  Entrées : a un arbre binaire
  Sorties : un entier
  Spec : calcul recursivement la hauteur d'un arbre, la hauteur de l'arbre vide est 0. Pour les appels
    recursifs, on calcul les hauteurs des 2 sous arbres et on ne prends que le maximum
*)
let rec (height:abin->int) = fun a -> 
  match a with
    | Leaf -> 0
    | Node(i, a1, a2) -> 1 + max (height a1) (height a2);;

let testHeight = height arbreSimple;;
let _ = assert(height arbreVide = 0);;
let _ = assert(height arbreAUnElem = 1);;
let _ = assert(height arbreSimple = 4);;

(* 
  product :
  Entrées : a un arbre binaire
  Sorties : un entier
  Spec : calcul recursivement le produit des élèments de l'arbre, donne 1 pour 
    l'arbre vide
*)
let rec (product:abin->int) = fun a ->
  match a with
    | Leaf -> 1
    | Node(i,a1,a2) -> i * product a1 * product a2;;

let testProduct = product arbreSimple;;
let _ = assert(product arbreVide = 1);;
let _ = assert(product arbreAUnElem = 5);;
let _ = assert(product arbreSimple = 1200);;

(* 
  sum :
  Entrées : a un arbre binaire
  Sorties : un entier
  Spec : calcul recursivement la somme des élèments de l'arbre, donne 0 pour 
    l'arbre vide
*)
let rec (sum:abin->int) = fun a ->
  match a with
    | Leaf -> 0
    | Node(i,a1,a2) -> i + sum a1 + sum a2;;

let testSum = sum arbreSimple;;
let _ = assert(sum arbreVide = 0);;
let _ = assert(sum arbreAUnElem = 5);;
let _ = assert(sum arbreSimple = 30);;

(* 
  find :
  Entrées : a un arbre binaire, value un entier
  Sorties : un booleen
  Spec : vérifie si la value est dans l'arbre a. Pour cela on regarde si la valeur du noeud est
    la valeur cherchée, si ce n'est pas le cas, on cherche a gauche puis a droite. Si i=value, le ou etant 
    paresseux, on ne fera pas le reste des calculs recursifs
*)
let rec (find:abin->int->bool) = fun a -> fun value -> 
  match a with
    | Leaf -> false
    | Node(i,a1,a2) -> i = value || find a1 value || find a2 value;;

let testFind = find arbreSimple 10;;
let _ = assert(find arbreVide 0 = false);;
let _ = assert(find arbreAUnElem 0 = false);;
let _ = assert(find arbreAUnElem 5);;
let _ = assert(find arbreSimple 12);;
let _ = assert(find arbreSimple 3 = false);;

(* 
  maxA :
  Entrées : a un arbre binaire
  Sorties : un entier
  Spec : calcul la valeur max d'un arbre binaire, si l'arbre est vide, le min d'un entier est retrounée
*)
let rec (maxA:abin->int) = fun a ->
  match a with
    | Leaf -> min_int
    | Node(i,a1,a2) -> (max i (max (maxA a1) (maxA a2)));; (* max entre la racine et le max des ”maxA” des sous arbres *)

let testMax = maxA arbreSimple;;
let _ = assert( maxA arbreVide = min_int);;
let _ = assert( maxA arbreAUnElem = 5);;
let _ = assert( maxA arbreSimple = 12);;

(* Ne Fonctionne pas sur Coq !*)
(* Il y a la méthode avec le type Option UP, comme vue en TD*)

(* 
  countFullNode :
  Entrées : a un arbre binaire
  Sorties : un entier
  Spec : calcul le nombre de noeuds binaires, pour cela on sépart les cas en fonction de la valeur des fils
*)
let rec countFullNode = fun a ->
  match a with
  | Leaf | Node (_,Leaf,Leaf) -> 0
  | Node (e,Leaf,a) | Node (e,a,Leaf) -> countFullNode a
  | Node (e,g,d) -> 1 + (countFullNode g) + (countFullNode d);;

  let _ = assert( countFullNode arbreVide = 0 );;
  let _ = assert( countFullNode arbreAUnElem = 0 );;
  let _ = assert( countFullNode arbreSimple = 1 );;

(* 
  countFullNodeV2 :
  Entrées : a un arbre binaire
  Sorties : un entier
  Spec : calcul le nombre de noeuds binaires, pour cela, verifie pour chaque noeud si ses enfants sont des
    feuilles.
*)
let rec (countFullNodeV2:abin->int) = fun a ->
  match a with
    | Leaf -> 0
    | Node(i,a1,a2) -> if(a1 <> Leaf && a2 <> Leaf) then 1 + countFullNodeV2 a1 + countFullNodeV2 a2
                                                       else countFullNodeV2 a1 + countFullNodeV2 a2;;

let testCountFullNodeV2 = countFullNodeV2 arbreSimple;;
let _ = assert( countFullNodeV2 arbreVide = 0 );;
let _ = assert( countFullNodeV2 arbreAUnElem = 0 );;
let _ = assert( countFullNodeV2 arbreSimple = 1 );;


(* 2.2 *)

(* Exo 17 *)
let arbreBinaireSimple = Node(5,Node(4,Node(2,Leaf,Node(3,Leaf,Leaf)),Leaf),Node(12,Leaf,Leaf));;
let abin1 = Node (2,Leaf,Leaf);;
let abin2 = Node (10,Node(5,Leaf,Leaf),Leaf);;
let abinEx = Node(5,Node(3,Node(2,Leaf,Leaf),Leaf),Node(8,Node(6,Leaf,Node(7,Leaf,Leaf)),Node(9,Leaf,Leaf)));;

(* 2.2.1 *)

(* Exo 18 *)

(* 
  mem :
  Entrée : a un abin et value un entier
  Sortie : un boolean
  Spec : Fonction qui cherche si l'entier est present ou non. Pour cela, on verifie sur chaque noeud
    si on est sur la valeur, si ce n'est pas le cas, on cherche a droite OU a gauche en fonction
    de cette meme valeur (val < racine -> gauche, sinon droite). Ceci est possible avec les proprietes
    des ABR. 
*)
let rec (mem:abin->int->bool) = fun a -> fun value ->
  match a with
    | Leaf -> false
    | Node(i,a1,a2) -> if(i = value) then true else
                    (if(i<value) then mem a2 value else mem a1 value);;

let testFindABR = mem arbreBinaireSimple 15;;
let _ = assert( mem arbreVide 12 = false);;
let _ = assert( mem abin1 12 = false);;
let _ = assert( mem abin1 2 = true);;
let _ = assert( mem abin2 10 = true);;
let _ = assert( mem arbreBinaireSimple 12 = true);;
let _ = assert( mem arbreBinaireSimple 13 = false);;

(* Exo 19 *)

(* 
  insert :
  Entrée : a un abin et value un entier
  Sortie : un abin
  Spec : Fonction qui insere l'entier dans l'ABR donne en parametre. Pour chaque noeud, on regarde dans
      quel sous arbre ranger la valeur et on fait l'insertion dans ce dernier . Si la valeur est deja dans
      l'arbre, une exception se lève
*)
let rec (insert:abin->int->abin) = fun a -> fun value ->
  match a with
    | Leaf -> Node(value,Leaf,Leaf)
    | Node(i,a1,a2) -> if (i=value) then failwith "Valeur deja présente" else
          if(i<value) then Node(i,a1,(insert a2 value))  else Node(i,(insert a1 value),a2);;

let testAddIntoABR = insert arbreBinaireSimple 15;;
let _ = assert(insert Leaf 1 = Node(1,Leaf,Leaf));;
let _ = assert(insert (insert abin1 1) 5 = Node(2,Node(1,Leaf,Leaf),Node(5,Leaf,Leaf)));;
let _ = assert(insert arbreBinaireSimple 15 = testAddIntoABR);;
let _ = assert(insert arbreBinaireSimple 15 <> arbreBinaireSimple);;

(* Exo 20 *)
(* Version barbare de vérification : beaucoups de parcours de l'arbre *)
(* sous fonction de calcul efficace du max d'un ABR *)
let rec (maxAbin:abin->int) = fun a ->
  match a with
  | Leaf -> failwith "arbreVide"
  | Node(e,_,Leaf) -> e
  | Node(_,_,d) -> maxAbin d;;
(* sous fonction de calcul efficace du min d'un ABR *)
let rec (minAbin:abin->int) = fun a ->
  match a with
  | Leaf -> failwith "arbreVide"
  | Node(e,Leaf,_) -> e
  | Node(_,g,_) -> minAbin g;;

(* 
  verif3 :
  Entrée : a un abin
  Sortie : un bool
  Spec : Fonction basic de virification de la prop d'ABR. On vérifie pour chaque noeud que ca valeur
    est bien comprise entre le max de son sous arbre de gauche et le min de son sous arbre de droite
*)
let rec (verif3:abin->bool) = fun a ->
  match a with
  | Leaf | Node(_,Leaf,Leaf) -> true
  | Node(e,g,Leaf) -> verif3(g) && e > (maxAbin g)
  | Node(e,Leaf,d) -> verif3(d) && e < (minAbin d)
  | Node(e,g,d) -> verif3(g) && verif3(d) && e > (maxAbin g) && e < (minAbin d);;

assert (verif3 Leaf = true);;
assert (verif3 abin1 = true);;
assert (verif3 abin2 = true);;
assert (verif3 (Node(3,Node(5,Leaf,Leaf),Leaf)) = false);;
assert (verif3 (Node(6,Node(3,Leaf,Leaf),Node(5,Leaf,Leaf))) = false);;
assert (verif3 (Node(4,Node(3,Leaf,Leaf),Node(5,Leaf,Leaf))));;
assert (verif3 abinEx = true) ;;
(* Fin version barbare *)

(* Version ou la verification de la propriétée se fait a la remontée des appels récursifs *)
(* on stock ici les infos suivantes : min, estABR, max *)
type estABinVal = int*bool*int;;
(* 
  verif2rec :
  Entrée : a un abin
  Sortie : un triplet (int,bool,int)
  Spec : Cette fonction vérifie a la remonté des appels récursifs si l'arbre est un ABR. Pour cela,
    chaque noeud va donné l'info du maximum et minimum de l'arbre qu'il représente ainsi qu'un 
    booléen disant si jusqu'a lui, l'arbre est un ABR
*)
let rec (verif2rec:abin->estABinVal) = fun a ->
  match a with
  | Leaf -> failwith "Should never appen"
  | Node(e,Leaf,Leaf) -> (e,true,e)
  | Node(e,ab,Leaf) -> let (mini,b,maxi) = (verif2rec ab) in (mini,b && e>maxi,e)
  | Node(e,Leaf,ab) -> let (mini,b,maxi) = (verif2rec ab) in (e,b && e<mini,maxi)
  | Node(e,g,d) -> let (miniG,bG,maxiG) = (verif2rec g) in 
          let (miniD,bD,maxiD) = (verif2rec d) in 
          (miniG,bG && bD && e>maxiG && e<miniD,maxiD);;
(* 
  verif2 :
  Entrée : a un abin
  Sortie : un bool
  Spec : Cette fonction initialise l'appel récursif de verification
*)
let (verif2:abin->bool) = fun a ->
  match a with
  | Leaf -> true
  | _ -> let (mini,b,maxi) = verif2rec(a) in b;;

assert (verif2 Leaf);;
assert (verif2 abin1);;
assert (verif2 abin2);;
assert (verif2 (Node(3,Node(5,Leaf,Leaf),Leaf)) = false);;
assert (verif2 (Node(6,Node(3,Leaf,Leaf),Node(5,Leaf,Leaf))) = false);;
assert (verif2 (Node(4,Node(3,Leaf,Leaf),Node(5,Leaf,Leaf))));;
assert (verif2 abinEx = true) ;;


(* Version ou la verification de la propriétée se fait a la descente des appels récursifs *)
(* 
  verifrec :
  Entrée : a un abin, min un int, max un int 
  Sortie : un triplet (int,bool,int)
  Spec : Cette fonction vérifie a la descente dans les appels récursifs que la prop d'ABR est vérifiée
    pour cela, on verifie que notre racine soit comprise entre le min et la max possible (CAD inférieur 
    ou supérieur au parent en fct de la postion dans l'arbre)
*)
let rec (verifrec:abin->int->int->bool) = fun a min max->
  match a with
  | Leaf -> failwith "Should never appen"
  | Node(e,Leaf,Leaf) -> e > min && e < max
  | Node(e,g,Leaf) -> e > min && e < max && (verifrec g min e)  (* e est le nouveau max *) 
  | Node(e,Leaf,d) -> e > min && e < max && (verifrec d e max)  (* e est le nouveau min *)
  | Node(e,g,d) -> (e > min && e < max) && (verifrec g min e) && (verifrec d e max) ;; (* e est le nouveau max et min*)

(* 
  verif :
  Entrée : a un abin
  Sortie : un bool
  Spec : Cette fonction initialise l'appel récursif de verification. Les min et max initiaux sont ceux 
    des entiers
*)
let (verif:abin->bool) = fun a ->
  match a with
  | Leaf -> true
  | _ -> verifrec a min_int max_int;;


assert (verif Leaf = true);;
assert (verif abin1 = true);;
assert (verif abin2 = true);;
assert (verif (Node(3,Node(5,Leaf,Leaf),Leaf)) = false);;
assert (verif (Node(6,Node(3,Leaf,Leaf),Node(5,Leaf,Leaf))) = false);;
assert (verif (Node(4,Node(3,Leaf,Leaf),Node(5,Leaf,Leaf))));;
assert (verif abinEx = true) ;;

(* 2.2.2 *)

(* Exo 21 *)

type listl = | Nil
             | Cons of int * listl;;

(* 
  createABR :
  Entrée : Une listl et un ABR vide
  Sortie : Un ABR contenant les information de la liste
  Spec : Fonction qui creer un ABR a partir d'une liste at d'un ABR vide
*)
let rec (createABR:listl->abin->abin) = fun l -> fun a ->
  match l with
    | Nil -> a
    | Cons(i,n) -> createABR n (insert a i);;

(* 
  concat :
  Entrée : Deux listl
  Sortie : Une listl avec le contenue des deux listes
  Spec : Fonction qui concatene deux listl
*)
let rec (concat:listl->listl->listl) = fun l1 -> fun l2 ->
  match l1 with
    | Nil -> l2
    | Cons(i,t1) -> Cons(i,(concat t1 l2));;

(* 
  triInfixe :
  Entrée : Un abin
  Sortie : Une listl
  Spec : Fonction qui met dans une liste les noeuds de l'ABR dans l'ordre croissant
*)
let rec (triInfixe:abin->listl) = fun a ->
  match a with
    | Leaf -> Nil
    | Node(i,a1,a2) -> concat (concat (triInfixe a1) (Cons(i,Nil))) (triInfixe a2);;

(* 
  triABR :
  Entrée : Une lislt 
  Sortie : Une listl
  Spec : Une fonction qui prend une liste de valeur et la met dans l'ordre croissant grace a un ABR
*)
let (triABR:listl->listl) = fun l ->
  match l with
    | Nil -> Nil
    | Cons(i,n) -> triInfixe (createABR l Leaf);;


let listTest = Cons(6,Cons(8,Cons(2,Cons(1,Cons(10,Cons (7, Nil))))));;
let listTestTrier = Cons(1,Cons(2,Cons(6,Cons(7,Cons(8,Cons (10, Nil))))));;
let testTri = triABR listTest;;
let _ = assert(triABR listTest = listTestTrier);;

(* Version 2 du trie ABR *)
(* 
listToABR :
Entrée : Une listl et un ABR 
Sortie : Un ABR
Spec : ajoute tous les élèments de la liste dans l'abre
*)
let rec listToABR = fun l a ->
  match l with
  | [] -> a
  | x::s -> listToABR s (insert a x);;

(* 
parcours :
Entrée : Un arbre
Sortie : Une listl
Spec : parcours l'arbre construit de facon infixe en ajoutant les éléments a une liste d'entiers
*)
let rec parcours = fun a->
  match a with
  | Leaf -> []
  | Node(x,g,d) -> (parcours g) @ [x] @ (parcours d);;

(* 
triABR :
Entrée : Une listl 
Sortie : Une listl
Spec : Une fonction qui prend une liste de valeur et la met dans l'ordre croissant grace a un ABR
*)
let triABRV2 = fun l -> parcours (listToABR l Leaf);;

let _ = assert (triABRV2 [16;28;9;15;3;8;0] = [0;3;8;9;15;16;28]);;


(* Exo 22 : BONUS *)

let rec verifExc = fun a ->
  match a with
  | Leaf -> failwith "Arbre vide"
  | Node(x,Leaf,Leaf) -> (x,x)
  | Node(x,g,Leaf) -> let (min,max) = (verifExc g) in if(max > x) then failwith "Pas ABR" else (min,x)
  | Node(x,Leaf,d) -> let (min,max) = (verifExc d) in if(min < x) then failwith "Pas ABR" else (x,max)
  | Node(x,g,d) -> let (minG,maxG) = (verifExc g) in let (minD,maxD) = (verifExc d) in if(x < maxG || x > minD) then failwith "Pas ABR" else (minG,maxD);;

assert (verifExc abin1 = (2,2));;
assert (verifExc abin2 = (5,10));;
assert (verifExc (Node(4,Node(3,Leaf,Leaf),Node(5,Leaf,Leaf))) = (3,5));;
assert (verifExc abinEx = (2,9)) ;;
