(* 
  BRUN Samuel & LITHAUD Alexandre
  
  Temps : 5-6h
  Pas fait : 
    - Exo 8.3 avec combinateurs d'analyseurs 
    - 8.3.6 : reconnaissance de la victoire en diagonale 
        (On y est presque mais on a manqué un peu de temps, il faudrait juste terminer les fonctions diagoToLigne qui rends une list de list correspondant aux différentes diagonales
        d'une matrice)

  Fait : tout le reste, aucune question notable

*)





(* ==================================  Initiation  ================================== *)

(* Transforme une string en liste (fonction fournie)*)
let list_of_string s =
  let n = String.length s in
  let rec boucle i =
    if i = n then [] else s.[i] :: boucle (i+1)
  in boucle 0;;

(** Le type des fonctions qui épluchent une liste de caractères. *) 
type analist = char list -> char list;;

(** Le type des fonctions qui épluchent une liste de caractères et rendent un résultat. *) 
type 'res ranalist = char list -> 'res * char list;;

(** L’exception levée en cas de tentative d’analyse ratée. *) 
exception Echec;;

(** Ne rien consommer *)
let epsilon : analist = fun l -> l;;
(** Un epsilon informatif *)
let epsilon_res (info : 'res) : 'res ranalist = fun l -> (info, l);;
(** Terminaux *)
let terminal c : analist = fun l -> 
  match l with
  | x :: l when x = c -> l 
  | _ -> raise Echec;;
let terminal_cond (p : char -> bool) : analist = fun l ->
   match l with 
  | x :: l when p x -> l
  | _ -> raise Echec;;
(** Le même avec résultat *)
(** f ne retourne pas un booléen mais un résultat optionnel *) 
let terminal_res (f : 'term -> 'res option) : 'res ranalist = fun l -> 
  match l with
  | x :: l -> (match f x with Some y -> y, l | None -> raise Echec) 
  | _ -> raise Echec;;


(* ==================================  Exo 8.1  ================================== *)
(* Question 1 *)
(*
Grammaire :
U = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"  
C = A | epsilon  
A = UC
*)


(* Question 2 *)

(* Analyseur de la grammaire *)
let (est_chiffre:char->bool) = fun x -> x <= '9' && x >= '0';;

let (p_U:analist) = terminal_cond est_chiffre;;
let rec (p_A:analist) = fun l -> 
  let l = p_U l in 
  let l = p_C l in 
  l
and (p_C:analist) = fun l -> 
  try p_A l 
  with Echec -> epsilon l;;

(* Fonction *)
let (chiffre:analist) = p_C;;
assert (p_C ['d';'A'] = ['d';'A']) ;;
assert (p_C ['0';'d';'A'] = ['d';'A']) ;;
assert (p_C ['d';'0';'A'] = ['d';'0';'A']) ;;
assert (p_C ['1';'3'] = []) ;;
assert (p_C [] = []) ;;


(* Question 3 *)

(* Analyseur de la grammaire, version type option *)
let (valChiffre:char->int option) = fun c -> if (est_chiffre c) then Some (int_of_char c - int_of_char '0') else None;;

let (rp_U:int ranalist) = terminal_res (valChiffre);;
let rec (rp_A:int ranalist) = fun l -> 
  let (v1,l) = rp_U l in 
  let (v2,l) = rp_C l in 
  (v1+v2,l)
and (rp_C:int ranalist) = fun l -> 
  try rp_A l 
  with Echec -> epsilon_res 0 l;;

(* Fonction *)
let (sommechiffres:int ranalist) = rp_C;;
assert (sommechiffres ['d';'A'] = (0,['d';'A'])) ;;
assert (sommechiffres ['0';'d';'A'] = (0,['d';'A'])) ;;
assert (sommechiffres ['d';'0';'A'] = (0,['d';'0';'A'])) ;;
assert (sommechiffres ['1';'3'] = (4,[])) ;;
assert (sommechiffres [] = (0,[])) ;;

(* Question 4 *)
let rec (h_A:int->int ranalist) = fun a l -> 
  let (v,l) = rp_U l in 
  let (v2,l) = horner (a*10 + v) l in 
  (v2,l)
and (horner:int->int ranalist) = fun a l -> 
  try h_A a l 
  with Echec -> epsilon_res a l;;

assert (horner 42 ['d';'A'] = (42,['d';'A'])) ;;
assert (horner 0 ['0';'d';'A'] = (0,['d';'A'])) ;;
assert (horner 0 ['d';'0';'A'] = (0,['d';'0';'A'])) ;;
assert (horner 0 ['1';'3'] = (13,[])) ;;
assert (horner 0 [] = (0,[])) ;;


(* Question 5 *)
let rec pow = fun a b -> if(b=1) then a else pow (a*a) (b-1);;

let (valChiffre:char->int option) = fun c -> if (est_chiffre c) then Some (int_of_char c - int_of_char '0') else None;;

let (rp_U:int ranalist) = terminal_res (valChiffre);;

let rec (hf_A:(int->int) ranalist) = fun l -> 
  let (v,l) = rp_U l in 
  let (f,l) = horner_f l in
  ((fun a -> (pow a 10) + f v),l)
and (horner_f:(int->int) ranalist) = fun l -> 
  try hf_A l 
  with Echec -> epsilon_res (fun v->v) l;;

let a,b = (horner_f ['1';'2';'3';'d';'A']) in a 0;;

assert (horner 0 ['d';'A'] = (0,['d';'A'])) ;;
assert (horner 0 ['0';'d';'A'] = (0,['d';'A'])) ;;
assert (horner 0 ['d';'0';'A'] = (0,['d';'0';'A'])) ;;
assert (horner 0 ['1';'3'] = (13,[])) ;;
assert (horner 0 [] = (0,[])) ;;



(* ==================================  Exo 8.3  ================================== *)
let readfile : string -> string = fun nomfic ->
let ic = open_in nomfic in
really_input_string ic (in_channel_length ic);;

(*
Grammaire du Connect 6 :
P ::= noir | blanc
C ::= (P intint) 
Cl ::= ε|CCl
S ::= (intint)Cl
*)

(* Exo 1 *)
type joueur = 
  | Blanc
  | Noir;;
type couple = int * int;;
type coups = joueur * couple;;

type partie = couple * coups list;;

let partie = ((19,19),[(Noir,3,5)]);;

(* Exo 2*)

(** Le type des fonctions qui épluchent une liste et rendent un résultat. *) 
type ('a,'r) ranalist = 'a list -> 'r * 'a list;;
(** L’exception levée en cas de tentative d’analyse ratée. *) 
exception Echec;;

(** Un epsilon informatif *)
let epsilon_res (info : 'r) : ('a,'r) ranalist = fun l -> (info, l);;
(** Terminaux *)
let terminal_cond (p : char -> bool) : ('a,'r) ranalist = fun l ->
   match l with 
  | x :: l when p x -> (x,l)
  | _ -> raise Echec;;
(** Le même avec résultat *)
let terminal_res (f : 'term -> 'r option) : ('a,'r) ranalist = fun l -> 
  match l with
  | x :: l -> (match f x with Some y -> y, l | None -> raise Echec) 
  | _ -> raise Echec;;

type token = 
  | Pouvrante
  | Pfermante
  | Chiffre of int
  | Joueur of joueur;;

let al_Pouvrante =  fun l -> let (v,l) = terminal_cond (fun x->x='(') l in (Pouvrante,l);;
let al_Pfermante = fun l -> let (v,l) = terminal_cond (fun x->x=')') l in (Pfermante,l);;

(* reconnaissance d'un entier : outils *)
let (est_chiffre:char->bool) = fun x -> x <= '9' && x >= '0';;

let (valChiffre:char->int option) = fun c -> if (est_chiffre c) then Some (int_of_char c - int_of_char '0') else None;;

let (un_chiffre:('a,'r)ranalist) = terminal_res valChiffre;;

let rec (h_AC:int->('a,'r) ranalist) = fun a l -> 
  let (v,l) = un_chiffre l in 
  let (v2,l) = horner_C (a*10 + v) l in 
  (v2,l)
and (horner_C:int -> ('a,'r) ranalist) = fun a l -> 
  try h_AC a l 
  with Echec -> epsilon_res a l;;
(* fonctions *)
let (al_Chiffre:('a,'r)ranalist) = h_AC 0;;

let (al_Joueur:('a,'r)ranalist) = fun l ->
  try
    let (_,l) = terminal_cond (fun c->c='b') l in
    let (_,l) = terminal_cond (fun c->c='l') l in
    let (_,l) = terminal_cond (fun c->c='a') l in
    let (_,l) = terminal_cond (fun c->c='n') l in
    let (_,l) = terminal_cond (fun c->c='c') l in
    (Blanc,l)
  with Echec -> 
    let (_,l) = terminal_cond (fun c->c='n') l in
    let (_,l) = terminal_cond (fun c->c='o') l in
    let (_,l) = terminal_cond (fun c->c='i') l in
    let (_,l) = terminal_cond (fun c->c='r') l in
    (Noir,l);;

let (token_here:('a,'r) ranalist) = fun l -> 
  try
    al_Pouvrante l
  with Echec ->
  try 
    al_Pfermante l
  with Echec ->
  try 
    let (v,l) = al_Chiffre l in (Chiffre(v),l)
  with Echec ->
    let (v,l) = al_Joueur l in (Joueur(v),l);;

assert (token_here ['(';'5';')'] = (Pouvrante,['5';')']));;
assert (token_here ['3';'5';')'] = (Chiffre 35,[')']));;
assert (token_here ['1';'5';'4'] = (Chiffre 154,[]));;
assert (token_here [')';'5';')'] = (Pfermante,['5';')']));;
(* Doit echouer *)
token_here [];;
token_here ['b';'l';'s'];;
token_here ['b';'l';'9';'n';'c'];;
(* Fin echecs *)
assert (token_here ['b';'l';'a';'n';'c'] = (Joueur Blanc,[]));;
assert (token_here ['n';'o';'i';'r';'9'] = (Joueur Noir,['9']));;


let rec (espace:'a list->'a list) = fun l ->
  try
    let (v,l) = terminal_cond (fun c->c=' ') l in 
    espace l
  with Echec -> l;;
assert (espace [' ']=[]);;
assert (espace [' ';' ';'4']=['4']);;
assert (espace ['4']=['4']);;

let (next_token:('a,'r) ranalist) = fun l ->
  let (t,v) = token_here (espace l) in (t,v);;
assert (next_token ['(';'5';')'] = (Pouvrante,['5';')']));;
assert (next_token [' ';'3';'5';')'] = (Chiffre 35,[')']));;
assert (next_token ['1';'5';'4'] = (Chiffre 154,[]));;
assert (next_token [')';'5';')'] = (Pfermante,['5';')']));;
(* Doit echouer *)
next_token [];;
next_token [' ';'b';'l';'s'];;
next_token ['b';'l';'9';'n';'c'];;
(* Fin echecs *)
assert (next_token ['b';'l';'a';'n';'c'] = (Joueur Blanc,[]));;
assert (next_token ['n';'o';'i';'r';'9'] = (Joueur Noir,['9']));;
assert (next_token [' ';' ';'b';'l';'a';'n';'c'] = (Joueur Blanc,[]));;
assert (next_token ['n';'o';'i';'r';'9'] = (Joueur Noir,['9']));;

let rec (tokens:'a list-> token list) = fun l ->
  try
    let (t,l) = next_token l in 
    t::tokens l
  with Echec -> [];;

let partie = list_of_string "(19 19) (noir 3 5) (  blanc 5      8 ) (blanc  5 9) (   noir 3 4)   ( noir  3 6)";;
let tokPartie = tokens partie;;


(* ========================== Q3 ========================== *)
(* !!!! Ici on aurait pu réutiliser les fonctions écrite pour la Q2, mais pas soucis de claireté, nous les avons copié aussi ici !!!! *)
(** Un epsilon informatif *)
let epsilon_res (info : 'r) : ('a,'r) ranalist = fun l -> (info, l);;
(** Terminaux *)
let terminal_cond (p : 'a -> bool) : ('a,'r) ranalist = fun l ->
   match l with 
  | x :: l when p x -> (x,l)
  | _ -> raise Echec;;


(*
Grammaire : une partie : 
  Coo = Pouvrante . Chiffre . Chiffre . Pfermante
  Coups = Pouvrante . Joueur . Chiffre . Chiffre . Pfermante . Lcoups
  Lcoups = Coups | epsilon
  P = Coo . Lcoups
*)

(* Lecture d'un token parenthèse ouvrante *)
let (as_Pouvrante:('a,'r) ranalist) = terminal_cond (fun tok->(tok = Pouvrante));;
(* Lecture d'un token parenthèse fermante *)
let (as_Fermante:('a,'r) ranalist) = terminal_cond (fun tok->(tok = Pfermante));;
(* Lecture d'un token parenthèse Chiffre *)
let (as_Chiffre:('a,'r) ranalist) = terminal_cond (fun tok->(match tok with | Chiffre _ -> true | _ -> false));;
(* Lecture d'un token parenthèse Joueur *)
let (as_Joueur:('a,'r) ranalist) = terminal_cond (fun tok->(match tok with | Joueur _ -> true | _ -> false));;

(* Lecture d'un couple de coo (CAD, 2 entiers a la suite) *)
let (as_couple:(token,couple) ranalist) = fun l ->
  let (v1,l) = as_Chiffre l in 
  let (v2,l) = as_Chiffre l in 
  match v1,v2 with
  | Chiffre c1, Chiffre c2 -> ((c1,c2),l)
  | _ -> raise Echec;; (* ne devrait jamais passer ici *)

(* Reconnait le non-terminal Coo*)
let (lit_taille_plateau:((token,couple) ranalist)) = fun l ->
  let (_,l) = as_Pouvrante l in 
  let (c,l) = as_couple l in 
  let (_,l) = as_Fermante l in 
  (c,l);;

let (as_1coups:(token,coups) ranalist) = fun l ->
  let (_,l) = as_Pouvrante l in 
  let (tokJoueur,l) = as_Joueur l in
  let (coo,l) = as_couple l in
  let (_,l) = as_Fermante l in 
  match tokJoueur with
  | Joueur j -> ((j,coo),l)
  | _ -> raise Echec (* ne devrait jamais passer ici *)

let rec (as_coups:(token,coups list) ranalist) = fun l ->
  let (coup,l) = as_1coups l in 
  let (coups,l) = as_Lcoups l in
  (coup::coups,l)
and (as_Lcoups:(token,coups list) ranalist) = fun l ->
  try as_coups l 
  with Echec -> epsilon_res [] l;;

let (lit_partie:token list -> partie) = fun l ->
  let (t,l) = lit_taille_plateau l in 
  let (lcoups,l) = as_Lcoups l in 
  (t,lcoups);;

let partieEx = lit_partie tokPartie;;




(* ============= Q4 =============*)

type case = 
  | V (*vide*)
  | B (*galet blanc*)
  | N (*galet noir*);;
type plateau = case list list;;



(* ============= Q5 =============*)
let rec (get_l:plateau->int->(case list)) = fun p l ->
  if (l<0) then failwith ("l négatif") else
  match p with
  | [] -> failwith ("l en dehors de la taille tableau")
  | caseList::suitePlateau -> if (l=0) then caseList else get_l p (l-1);;

let rec (get_c:case list->int->case) = fun ligne c ->
  if (c<0) then failwith ("c négatif") else
  match ligne with
  | [] -> failwith ("c en dehors de la taille tableau")
  | case::suite -> if (c=0) then case else get_c suite (c-1);;

let (get_case_at:plateau->couple->case) = fun p (l,c) -> get_c (get_l p l) c;;

let (est_coup_valid:plateau->couple->bool) = fun p coo -> 
  match get_case_at p coo with
  | V -> true
  | _ -> false;;

let rec (set_c:case list->int->case->(case list)) = fun ligne c caseASet ->
  if (c<0) then failwith ("c négatif") else
  match ligne with
  | [] -> failwith ("c en dehors de la taille tableau")
  | case::suite -> if (c=0) then caseASet::suite else case::(set_c suite (c-1) caseASet);;

let rec (set_l:plateau->couple->case->plateau) = fun p (l,c) caseASet ->
  if (l<0) then failwith ("l négatif") else
  match p with
  | [] -> failwith ("l en dehors de la taille tableau")
  | caseList::suitePlateau -> if (l=0) then (set_c caseList c caseASet)::suitePlateau else caseList::(set_l suitePlateau ((l-1),c) caseASet);;

let (joue_coup:plateau->coups->plateau) = fun p (j,coo) ->
  if (est_coup_valid p coo) then 
    match j with
    | Blanc -> set_l p coo B
    | Noir -> set_l p coo N
  else
    failwith "Coup invalide";;


(* =============== Q6 *)
let rec (jouer_tous_coups:plateau->coups list->plateau)= fun p l ->
  match l with
  | [] -> p
  | c::s -> joue_coup (jouer_tous_coups p s) c;;

let rec (init_col:int->(case list)) = fun c ->
  if (c=0) then [] else V::(init_col (c-1));;
init_col 6;;

let rec (init_plateau:couple->plateau) = fun (l,c) ->
  if(l=0) then [] else (init_col c)::(init_plateau (l-1,c));;
init_plateau (6,6);;

let (jouer_partie:partie->plateau) = fun (taille,listCoups) ->
  jouer_tous_coups (init_plateau taille) listCoups;;

jouer_partie partieEx;;

type victoire =
  | BlancV
  | NoirV
  | PartieNull
  | PartieNonFinie;;

let rec (est_victoire_sur_ligne:(case list)->case->int->bool -> victoire) = fun ligne caseChaine lg existCaseVide ->
  match ligne with
  | [] -> if(lg = 6) then match caseChaine with | B -> BlancV | N -> NoirV |_ -> PartieNonFinie
          else if existCaseVide then PartieNonFinie else PartieNull
  | case::suite -> if (lg = 6) then match caseChaine with | B -> BlancV | N -> NoirV |_ -> PartieNonFinie
                  else if (case = V) then est_victoire_sur_ligne suite V 0 true 
                  else if (caseChaine = V) then est_victoire_sur_ligne suite case 1 existCaseVide
                  else if (case = caseChaine) then est_victoire_sur_ligne suite case (lg+1) existCaseVide
                  else est_victoire_sur_ligne suite case 1 existCaseVide;;

est_victoire_sur_ligne [V;V;B;B;B;B;B;B] V 0 false;;
est_victoire_sur_ligne [B;B;B;B;B;B] V 0 false;;
est_victoire_sur_ligne [N;N;N;N;N;N] V 0 false;;
est_victoire_sur_ligne [N;N;N;V;N;N;N] V 0 false;;
est_victoire_sur_ligne [N;N;N;N;N;B] V 0 false;;
est_victoire_sur_ligne [N;N;N;N;N;B;V] V 0 false;;

let rec (col_to_ligne:plateau->int->(case list)) = fun p c ->
  match p with
  | [] -> []
  | l::suite -> (get_c l c)::(col_to_ligne suite c);;


let rec (checkLigne:plateau->victoire) = fun p ->
  match p with
  | [] -> failwith "plateau vide"
  | [l] -> est_victoire_sur_ligne l V 0 false
  | l::s -> let res = (est_victoire_sur_ligne l V 0 false) in 
        match res with
        | PartieNull -> checkLigne s
        | PartieNonFinie -> (let rSuite = checkLigne s in match rSuite with | PartieNull -> PartieNonFinie | _ -> rSuite)
        | _ -> res;;

(* inverse aussi mais c'est pas grave, l'ordre importe pas*)
let rec (transpo:plateau->int->plateau) = fun p i ->
  if (i=0) then [] else (col_to_ligne p i)::(transpo p (i-1));;

let rec (checkCol:plateau->couple->victoire) = fun p (l,c) -> checkLigne (transpo p c);;



(* FONCTIONS NON - ACHEVEES : MANQUE DE TEMPS  *)
(* diago de haut gauche vers bas droite *)
let rec (diagoToLigne:plateau->couple->(case list)) = fun p (l,c) ->
  if(c=0) then (get_case_at p (l,c))::[] else (get_case_at p (l,c))::(diagoToLigne p (l+1,c-1));;

let rec (toutDiagoToLigne_rec:plateau->couple->int->(case list list)) = fun p (l,c) i ->
  if(i=0) then (diagoToLigne p (0,0)) :: [] else (diagoToLigne p (0,i)) :: (toutDiagoToLigne_rec p (l,c) (i-1));;

let (toutDiagoToLigne:plateau->couple->(case list list)) = fun p (l,c) -> toutDiagoToLigne_rec p (l,c) (l*c-1);;
toutDiagoToLigne [[B;N;N;N;B;N;V];
                  [N;N;N;B;B;N;V];
                  [N;B;B;N;N;N;B];
                  [N;N;N;N;B;N;V];
                  [N;N;N;N;B;N;V];
                  [N;N;B;N;B;N;V]] (6,6);;
(* diago de bas gauche vers haut droite *)
let rec (diagoToLigne:plateau->couple->(case list)) = fun p (l,c) ->
  if(c=0) then (get_case_at p (l,c))::[] else (get_case_at p (l,c))::(diagoToLigne p (l-1,c+1));;

let rec (toutDiagoToLigne_rec:plateau->couple->int->(case list list)) = fun p (l,c) i ->
  if(i=0) then (diagoToLigne p (0,0)) :: [] else (diagoToLigne p (0,i)) :: (toutDiagoToLigne_rec p (l,c) (i-1));;

let (toutDiagoToLigne:plateau->couple->(case list list)) = fun p (l,c) -> toutDiagoToLigne_rec p (l,c) (l*c-1);;


(* Reconnais uniquement une victoire sur les lignes OU colonnes *)
let (resultat:plateau->couple->victoire) = fun p taille ->
  let r = (checkLigne p) in
  match r with
  | PartieNull | PartieNonFinie -> (
      let r = (checkCol p taille) in 
      match r with 
      | PartieNull | PartieNonFinie -> r (* mettre le check diago*)
      | _ -> r)
  | _ -> r;;

resultat [[B;N;N;N;B;N;V];
          [N;N;N;B;B;N;V];
          [N;B;B;N;N;N;B];
          [N;N;N;N;B;N;V];
          [N;N;N;N;B;N;V];
          [N;N;B;N;B;N;V]] (6,6);;