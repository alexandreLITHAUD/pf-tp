
(* ================= 6 Analyse lexicale et syntaxique ================= *)

(*
  Samuel Brun
  Alexandre Lithaud
*)

let readfile : string -> string = fun nomfic ->
let ic = open_in nomfic in
really_input_string ic (in_channel_length ic);;


let analistt = readfile("analist.ml");;

let anacomb = readfile("anacomb.ml");;

module Analis = struct
  analistt
end;;

module Anaco = struct
  anacomb
end;;

(* =================  6.1 Préliminaires =================  *)

(* 1. *)

(*
Grammaire :

U ::= '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'

A ::= U . C

C ::= A | Epsilon

*)

let (val_chiffre:char->int) = fun c -> int_of_char c - int_of_char '0';;

let le_chiffre = fun x -> if(x ='0' || x ='1' || x='2' || x ='3' || x ='4' || x='5' || x ='6' || x ='7' || x='8' || x='9') then Some (val_chiffre x) else None;;

let rp_U = terminal_res(le_chiffre);;

let rec rp_A = fun l -> let(n,l)= rp_U l in 
                    let(m,l) = rp_C l in (n+m,l)
and rp_C = fun l -> try rp_A l with Echec -> epsilon_res 0 l;;


(* =================  6.2 Connect6 =================  *)


(*
(* Le type des fonctions qui épluchent une liste de caractères. *)
type analist = char list -> char list;;
(* Le type des fonctions qui épluchent une liste de caractères et rendent un résultat. *)
type 'res ranalist = char list -> 'res * char list;;
(* L’exception levée en cas de tentative d’analyse ratée. *)
exception Echec;;
(* Ne rien consommer *)
let epsilon : analist = fun l -> l;;
(* Un epsilon informatif *)
let epsilon_res (info : 'res) : 'res ranalist =
fun l -> (info, l);;
(* Terminaux *)
let terminal c : analist = fun l -> match l with
| x :: l when x = c -> l
|
 _ -> raise Echec;;
let terminal_cond (p : char -> bool) : analist = fun l -> match l with
| x :: l when p x -> l
|
_ -> raise Echec;;
(* Le même avec résultat *)
(* f ne retourne pas un booléen mais un résultat optionnel *)
let terminal_res (f : 'term -> 'res option) : 'res ranalist =
fun l -> match l with
| x :: l -> (match f x with Some y -> y, l | None -> raise Echec)
| _ -> raise Echec;;
*)


(*Exo 8.1*)



(* 4. *)

(* 5. *)


(* COMPTEUR *)

let (compteur:unit->int) =
  let c = ref 0 in
  let compt = fun _ ->
    let v = !c in
    let _ = c := v+1 in v
in compt;;

let e = compteur ();;
