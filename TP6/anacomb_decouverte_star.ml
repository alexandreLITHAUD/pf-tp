(* Analyse descendante récursive sur une liste avec des combinateurs *)
(* Complément à anacom_decouverte.ml, dédié à l'étoire de Kleene     *)
(* Dans toute la suite, les listes sont implicitement des
   listes de caractères. *)

(* Utilitaire pour les tests *)

let list_of_string s =
  let n = String.length s in
  let rec boucle i =
    if i = n then [] else s.[i] :: boucle (i+1)
  in boucle 0

(* ============================================================ *)
(* Échauffement : combinateurs d'analyseurs purs                *)
(* ------------------------------------------------------------ *)

(* Le type des aspirateurs de listes de caratères  *)
type analist = char list -> char list

exception Echec

(* terminal constant *)
let terminal c : analist = fun l -> match l with
  | x :: l when x = c -> l
  | _ -> raise Echec

(* terminal conditionnel *)
let terminal_cond (p : 'term -> bool) : analist = function
  | x :: l when p x -> l
  | _ -> raise Echec

(* non-terminal vide *)
let epsilon : analist = fun l -> l

(* Composition séquentielle : a suivi de b *)
let (-->) (a : analist) (b : analist) : analist =
  fun l -> let l = a l in b l

(* Choix entre a ou b *)
let (-|) (a : analist) (b : analist) : analist =
  fun l -> try a l with Echec -> b l

(* Répétition (étoile de Kleene) *)
(* Remarque : l'opétateur étoile de Kleene est défini récursivement *)
(* Grammaire :  A* ::=  A A* | ε *)
let rec star (a : analist) : analist = fun l -> l |>
  ( a --> star a ) -| epsilon

(* Exemple d'utilisation *)
let est_chiffre : char -> bool = function
  | '0' .. '9' -> true
  | _ -> false

(* (n,)*  où n est un chiffre *)
let p_repet : analist = star (terminal_cond est_chiffre --> terminal ',')
let _ = p_repet (list_of_string "1,2,345")
       
(* ==================================================================== *)
(* Combinateurs d'analyseurs avec calcul supplémentaire (ex. un AST)    *)
(* -------------------------------------------------------------------- *)

(* Le type des aspirateurs de listes qui rendent un résultat de type 'res *)
type 'res ranalist = char list -> 'res * char list

let epsilon_res (info : 'res) : 'res ranalist =
  fun l -> (info, l)

(* Terminal conditionnel avec résultat *)
(* [f] ne retourne pas un booléen mais un résultat optionnel *)
let terminal_res (f : char -> 'res option) : 'res ranalist =
  fun l -> match l with
  | x :: l -> (match f x with Some y -> y, l | None -> raise Echec)
  | _ -> raise Echec

(* Choix entre a ou b informatifs *)
let (+|) (a : 'res ranalist) (b : 'res ranalist) : 'res ranalist =
  fun l -> try a l with Echec -> b l

(* Composition séquentielle *)
(* a sans résultat suivi de b donnant un résultat *)
let ( -+>) (a : analist) (b : 'res ranalist) : 'res ranalist =
  fun l -> let l = a l in b l

(* a rendant un résultat suivi de b rendant un résultat *)
let (++>) (a : 'resa ranalist) (b : 'resa -> 'resb ranalist) :
      'resb ranalist =
  fun l -> let (x, l) = a l in b x l

(* a rendant un résultat suivi de b sans résultat *)
let (+->) (a : 'res ranalist) (b : analist) :
      'res ranalist =
  fun l -> let (x, l) = a l in x, b l

(* ----------------------------- *)
(* Répétition (étoile de Kleene) *)
let (<<) f g = fun x -> f (g x)
let (>>) f g = fun x -> g (f x)

(* Pipeline right to left*)
let star_pipe_R2L (a : ('r -> 'r) ranalist) : ('r -> 'r) ranalist =
  let rec a_star = fun l ->
    ( ( a ++> fun f -> a_star ++> fun f_star -> epsilon_res (f << f_star) )
      +|
        epsilon_res (fun x -> x)
    ) l
  in a_star

let star_R2L (a : ('r -> 'r) ranalist) (r0 : 'r) : 'r ranalist =
  star_pipe_R2L a ++> fun f -> epsilon_res (f r0)

(* Special case: building lists *)
let star_list (a : 'a ranalist) : ('a list) ranalist =
  star_R2L (a ++> fun x -> epsilon_res (fun l -> x :: l)) []

(* Pipeline left to right*)
let star_pipe_L2R (a : ('r -> 'r) ranalist) : ('r -> 'r) ranalist =
  let rec a_star = fun l ->
    ( ( a ++> fun f -> a_star ++> fun f_star -> epsilon_res (f >> f_star) )
      +|
        epsilon_res (fun x -> x)
    ) l
  in a_star

let star_L2R (r0 : 'r) (a : ('r -> 'r) ranalist) : 'r ranalist =
  star_pipe_L2R a ++> fun f -> epsilon_res (r0 |> f)

(* ----------------------------- *)
(* Exemple avec étoile de Kleene *)

let valchiffre : char -> int option = fun c ->
  match c with
  | '0' .. '9' -> Some (Char.code c - Char.code '0')
  |_ -> None

let chiffre : int ranalist = terminal_res valchiffre
let pluschiffre : (int -> int) ranalist =
  chiffre ++> fun n -> epsilon_res ((+) n)
let plus10chiffre : (int->int) ranalist =
  chiffre ++> fun n -> epsilon_res (fun x -> 10 * x + n)
let sommechiffres : int ranalist = star_R2L pluschiffre 0
let horner : int ranalist = star_L2R 0 plus10chiffre

let _ = sommechiffres (list_of_string "123-reste")
let _ = horner (list_of_string "123-reste")
