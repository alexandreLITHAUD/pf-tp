(* TODO : finir les exos 22,23,24,25 *)

(* Retour sur l'exo 16 *)
type listent = 
  | Nil 
  | Cons of int * listent;;

let rec longueurL = fun l -> 
  match l with
  | Nil -> 0
  | Cons(x, s) -> 1 + longueurL s;;

let rec sommeL = fun l ->
  match l with
  | Nil -> 0
  | Cons (x,s) -> x + sommeL s;;

let rec positifL = fun l ->
  match l with 
  | Nil -> true
  | Cons (x,s) -> (x>0)&&(positifL s);;

let rec ajoutFinL = fun l e ->
  match l with
  | Nil -> Cons (e,Nil)
  | Cons (x,s) -> Cons (x, ajoutFinL s e);;

let rec concatL = fun l1 l2 ->
  match l1 with
  | Nil -> l2
  | Cons (x,s) -> Cons (x, concatL s l2);;




(*  EXO  26  *)
exception ListeVide;;
(*Partie A*)
let rec trouver_min_i_rec = fun l m c ->
  match l with
  | [] -> (m,c)
  | x :: s -> if (x<m) then trouver_min_i_rec s x (m::c)
                    else trouver_min_i_rec s m (x :: c);; 

let (trouve_min_i:int list -> int*int list) = fun l ->
  match l with
  | [] -> raise ListeVide
  | x :: s -> trouver_min_i_rec s x [];;

let (a,_) = trouve_min_i [1;2;7;0;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_i [1;2;7;10;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_i [1;2;7;10;82;7;9] in assert (a=1);;
let (a,_) = trouve_min_i [10;2;7;12;82;7;1] in assert (a=1);;

(*Partie B*)
let rec trouve_min_rec = fun comp l m c ->
  match l with
  | [] -> (m,c)
  | x::s -> if (comp x m) then trouve_min_rec comp s x (m::c)
                          else trouve_min_rec comp s m (x::c);;

let (trouve_min:('a->'a->bool)->('a list) -> 'a * ('a list)) = fun comp l ->
  match l with
  | [] -> raise ListeVide
  | x :: s -> trouve_min_rec comp s x [];;

let (a,_) = trouve_min (fun a b->a>b) [1;2;7;0;82;7;0] in assert (a=82);;
let (a,_) = trouve_min (fun a b->a<b) [1;2;7;0;82;7;0] in assert (a=0);;
let (a,_) = trouve_min (fun a b->a<b) [2;2;2;2;2;2;2] in assert (a=2);;

(*Partie C*)
let trouve_min_iV2 = fun l -> trouve_min (fun a b->a<b) l;;
(* Equivalent a : let trouve_min_iV2 = trouve_min (fun a b->a<b);;*)
let (a,_) = trouve_min_iV2 [1;2;7;0;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_iV2 [1;2;7;10;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_iV2 [1;2;7;10;82;7;9] in assert (a=1);;
let (a,_) = trouve_min_iV2 [10;2;7;12;82;7;1] in assert (a=1);;

(*Partie D : facultative*)
type typeObjet =
  | LecteurMP3
  | AppareilPhoto
  | Camera
  | Telephone
  | Ordinateur;;

type marque = 
  | Alpel
  | Syno
  | Massung
  | Liphisp;;

type article = typeObjet * marque * int * int;;
let (trouve_min_a:article list->article * article list) = 
  trouve_min (fun (t1,m1,p1,n1) (t2,m2,p2,n2) -> p1 < p2);;

trouve_min_a [(Ordinateur,Alpel,1000,10);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,10)];;


(*Exo 27*)
let rec (tri_selection:('a -> 'a -> bool) -> 'a list -> 'a list) =fun comp l->
  match l with
  | [] -> []
  | x::s -> let (a,b) = (trouve_min comp (x::s)) in a::(tri_selection comp b);;

tri_selection (fun a b->a<b) [];;
tri_selection (fun a b->a<b) [3;9;0;1;2;34;7;10];;

let tri_selection_i = tri_selection (fun a b->a<b);;
tri_selection_i [3;9;0;1;2;34;7;10];;

let (tri_selection_a:article list->article list) = tri_selection (fun (t1,m1,p1,n1) (t2,m2,p2,n2) -> p1 < p2);;
tri_selection_a [(Ordinateur,Alpel,1000,10);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,10)];;

(* Exo 28 *)
let max_int_alea = 1000000;;

let rec (liste_alea:int->(int list)) = fun n ->
  if n = 0 then [] else (Random.int max_int_alea) ::(liste_alea (n-1));;

(*
Random.self_init;;
*)
Random.init 1;;
liste_alea 5;;
liste_alea 9;;

(* Exo 29 *)

type abin =
  | Leaf
  | Node of int * abin * abin;;

let rec (insert:abin->int->abin) = fun a -> fun value ->
  match a with
    | Leaf -> Node(value,Leaf,Leaf)
    | Node(i,a1,a2) -> if (i=value) then failwith "Valeur deja présente" else
          if(i<value) then Node(i,a1,(insert a2 value))  else Node(i,(insert a1 value),a2);;

(* 
listToABR :
Entrée : Une listl et un ABR 
Sortie : Un ABR
Spec : ajoute tous les élèments de la liste dans l'abre
*)
let rec listToABR = fun l a ->
  match l with
  | [] -> a
  | x::s -> listToABR s (insert a x);;

(* 
parcours :
Entrée : Un arbre
Sortie : Une listl
Spec : parcours l'arbre construit de facon infixe en ajoutant les éléments a une liste d'entiers
*)
let rec parcours = fun a->
  match a with
  | Leaf -> []
  | Node(x,g,d) -> (parcours g) @ [x] @ (parcours d);;

(* 
triABR :
Entrée : Une listl 
Sortie : Une listl
Spec : Une fonction qui prend une liste de valeur et la met dans l'ordre croissant grace a un ABR
*)
let triABRV2 = fun l -> parcours (listToABR l Leaf);;

let lalea = liste_alea 1000;;
let deb = Sys.time() in
  let _ = triABRV2 lalea
    in Sys.time() -. deb;;
let deb = Sys.time() in
  let _ = tri_selection_i lalea
    in Sys.time() -. deb;;


let rec genereTrierInverse = fun n->
  if n = 0 then [] else n::(genereTrierInverse (n-1));;

let _ = genereTrierInverse 10;;


let rec genereTrier = fun n i->
  if n = i then [] else (i+1)::(genereTrier n (i+1));;

let _ = genereTrier 10 0;;




(* PARTIE 3.3 *)

(* Exo 30 *)
let rec (renv:'a list->'a list) = fun l ->
  match l with
  | [] -> []
  | x :: s -> (renv s) @ [x] ;;

renv [];;
renv [2;4;6;7];;

(* Exo 31 *)

let rec (renv_app:'a list->'a list->'a list) = fun l1 l2 ->
  match l1 with
  | [] -> l2
  | x :: s -> renv_app s (x::l2);;

let (renvV2:'a list->'a list) = fun l -> renv_app l [];;
renvV2 [];;
renvV2 [2;4;6;7];;

let l1 = liste_alea 10000;;

let deb = Sys.time() in
  let _ = renv l1
    in Sys.time() -. deb;;

let deb = Sys.time() in
  let _ = renvV2 l1
    in Sys.time() -. deb;;

(* TEMA LA TAILLE DE LA DIFFERENCE DE TEMPS 
   renv classique : O(n^3)
   renv opti : O(n) *)

(* Exo 33 *)
let l1 = liste_alea 10000;;
let l2 = liste_alea 10000;;
let l3 = liste_alea 10000;;


let concat_r = Sys.time() in
let _ = l1 @ (l2 @ l3)
in Sys.time() -. concat_r;;
(* 2n *)


let concat_l = Sys.time() in
let _ = (l1 @ l2) @ l3
in Sys.time() -. concat_l;;
(* n*n *)

(* PARTIE 3.4 *)
(* exo 34 *)
let rec (dromadaire:('a->'a->bool)->('a list)->'a) = fun comp l ->
  match l with
  | [] -> raise ListeVide
  | x :: [] -> x
  | x :: s -> let a = dromadaire comp s in (if comp x a then x else a);;

assert(dromadaire (fun a b->a>b) [1;2;4;0;8;19]=19);;
assert(dromadaire (fun a b->a<b) [1;2;4;0;8;19]=0);;

let rec chameau = fun comp l ->
  match l with
  | [] | _ :: [] -> raise ListeVide
  | x1 :: x2 :: [] -> if comp x1 x2 then (x1,x2) else (x2,x1)
  | x :: s -> let (a,b)=chameau comp s in 
              if(comp x a) then (x,a) else
                (if (comp x b) then (a,x) else (a,b));;

              
chameau (fun a b->a>b) [1;2;4;0;8;19];;
assert(chameau (fun a b->a>b) [1;2;4;0;8;19]=(19,8));;
assert(chameau (fun a b->a<b) [1;2;4;0;8;19]=(0,1));;

(* PARTIE 3.5 *)
(* Exo 36 *)
type produit =
  | LecteurMP3
  | AppareilPhoto
  | Camera
  | Telephone
  | Ordinateur;;

type marque = 
  | Alpel
  | Syno
  | Massung
  | Liphisp;;

type prix = int;;

type stock = int;;

type article = produit * marque * prix * stock;;

let s1 = [(Ordinateur,Alpel,1000,10);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,1);(Telephone,Massung,100,0)]

(* Exo 37 *)

let (memeProduit:produit->marque->prix->produit->marque->prix->bool) = fun t1 m1 p1 t2 m2 p2 ->
  (t1 = t2) && (m1 = m2) && (p1 = p2);;

let rec (est_en_stock:produit->marque->prix->(article list)->bool) = fun t m p l -> 
  match l with
  | [] -> false
  | x :: s -> let (pro,mar,pri,sto) = x in 
          (if memeProduit t m p pro mar pri then sto>0 else (est_en_stock t m p s));;

assert (est_en_stock Telephone Massung 430 s1 = true);;
assert (est_en_stock Telephone Massung 100 s1 = false);;
assert (est_en_stock Telephone Alpel 430 s1 = false);;

(* Exo 38 *)

let rec (ajoute_article:article->(article list)->(article list)) = fun (t,m,p,s) l ->
  match l with
  | [] -> [(t,m,p,s)]
  | (t',m',p',s')::suite -> if( memeProduit t m p t' m' p' ) 
            then (t',m',p',s'+s)::suite
            else (t',m',p',s')::(ajoute_article (t,m,p,s) suite);;
 
ajoute_article (Telephone,Massung,430,1) s1;;
ajoute_article (Telephone,Massung,100,10) s1;;
ajoute_article (Telephone,Alpel,1500,16) s1;;


(* Exo 39 *)
let rec (enleve_article:article list->article->(article list)) = fun l (t,m,p,s) ->
  match l with
  | [] -> []
  | (t',m',p',s') :: suite -> if( memeProduit t m p t' m' p') 
            then suite else (t',m',p',s')::(enleve_article suite (t,m,p,s));;

enleve_article s1 (Telephone,Massung,430,1);;
enleve_article s1 (Telephone,Massung,100,10);;
enleve_article s1 (Telephone,Alpel,1500,16);;


(* Exo 40 *)
let rec (ces_produits:produit->(article list)->(article list)) = fun prod l ->
  match l with
  | [] -> []
  | (t,m,p,s) :: suite -> if(t = prod) then (t,m,p,s)::(ces_produits prod suite) 
          else (ces_produits prod suite);;

let s2 = [(Ordinateur,Alpel,1000,10);(Telephone,Alpel,430,1);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,1);(Telephone,Massung,100,0)];;

ces_produits Telephone s2;;
ces_produits Ordinateur s1;;
ces_produits LecteurMP3 s1;;


(* Exo 41 *)

let lePlusCourantV1 = fun p l -> chameau (fun (t1,m1,p1,s1) (t2,m2,p2,s2) -> p1<p2) (ces_produits p l);;



let rec (lesPlusCourantProd2:produit->article list->article->article->article) = fun p l (t1,m1,p1,s1) (t2,m2,p2,s2) ->
  match l with
  | [] -> (t2,m2,p2,s2)
  | (t,m,pr,s) :: suite -> if(p=t)then 
                                (if (pr<p1) 
                                    then lesPlusCourantProd2 p suite (t,m,pr,s) (t1,m1,p1,s1) 
                                else  
                                    (if (pr<p2) 
                                        then lesPlusCourantProd2 p suite (t1,m1,p1,s1) (t,m,pr,s) 
                                    else lesPlusCourantProd2 p suite (t1,m1,p1,s1) (t2,m2,p2,s2))
                                )
                            else (lesPlusCourantProd2 p suite (t1,m1,p1,s1) (t2,m2,p2,s2));;

let rec (lesPlusCourantProd1:produit->article list->article->article) = fun p l (t1,m1,p1,s1) ->
  match l with
  | [] -> failwith(" Un seul article de type produit ")
  | (t,m,pr,s) :: suite -> if(p=t)then (if(pr<p1) then lesPlusCourantProd2 p suite (t,m,pr,s) (t1,m1,p1,s1) else lesPlusCourantProd2 p suite (t1,m1,p1,s1) (t,m,pr,s))
            else lesPlusCourantProd1 p suite (t1,m1,p1,s1);;

let rec (lesPlusCourant:produit->article list->article) = fun p l ->
  match l with
  | [] -> failwith(" Aucun article de type produit ")
  | (t,m,pr,s) :: suite -> if(p=t)then lesPlusCourantProd1 p suite (t,m,pr,s)
            else lesPlusCourant p suite;;

let s2 = [(Ordinateur,Alpel,1000,10);(Telephone,Alpel,430,1);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,1);(Telephone,Massung,100,0)];;
lesPlusCourant Telephone s2;;

(* Exo 42 *)
let rec (budget:int->int->article list->article list) = fun min max l ->
  match l with
  | [] -> []
  | (t,m,p,s) :: suite -> if( p >= min && p <= max)then (t,m,p,s)::budget min max suite 
        else budget min max suite;;

(* EXO 43 *)
let rec (achete:article list->produit->marque->prix->article list) = fun l t m p ->
  match l with
  | [] -> []
  | (t1,m1,p1,s1) :: suite -> if(memeProduit t m p t1 m1 p1) 
        then (t1,m1,p1,max (s1-1) 0)::suite
        else (t1,m1,p1,s1)::(achete suite t m p);;


(* Exo 44 *)
let rec (commande:article list->article list)= fun l ->
  match l with
  | [] -> []
  | (t1,m1,p1,s1) :: suite -> if(s1 <= 0)then (t1,m1,p1,s1)::(commande suite)
                              else commande suite;;
