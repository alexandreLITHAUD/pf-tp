(* TP 3 Programmation Fonctionelle *)

(* Exo 26 *)

let rec (tmin:int list->int) = fun l ->
  match l with
    | [] -> max_int
    | x::s -> min x (tmin s);;

let rec (rmList:'a list->'a->'a list) = fun l ->
  fun value ->
  match l with
    | [] -> []
    | x::s -> if(x=value) then (match s with
                               | [] -> []
                               | x2::s2 -> x2 :: s2
                               )  else x :: rmList s value;;

let (trouve_min_i:int list->int * int list) = fun l ->
  match l with
    | [] -> (0,[])
    | x::s -> (tmin l, rmList l (tmin l));;


let listeTest = [3;5;4;1;2];;

let test = trouve_min_i listeTest;;

(* let rec (trouve_min:('a->'a->bool)->'a list->'a * 'a list) = fun f l ->
  match l with
    | [] -> (0,[])
    | x::s -> (match trouve_min s with
                | [] -> x
                | x1::s1 -> if(f x x1) then x else x1
              , match trouve_min s with
                | [] -> 
                | x1::s1 ->
              );;

*)

(*let rec (tmin_gen:('a->'a->bool)->'a list->'a) = fun f l->
  match l with
    | [] -> 0
    | x::s -> (match tmin_gen s with
                | [] -> x
                | x1::s1 -> if(f x x1) then x else x1);;

let rec (rmList_gen:'a list->'a->'a list) = fun l ->
  fun value ->
  match l with
    | [] -> []
    | x::s -> if(x=value) then (match s with
                               | [] -> []
                               | x2::s2 -> x2 :: s2
                               )  else x :: rmList s value;;

*)



(* Exo 28 *)

let rec (random_list:int->int list) = fun i ->
if(i=0) then [] else  Random.int(1000) :: random_list(i-1);;

Random.self_init;;

let _ = random_list 5;;
let _ = random_list 5;;

let _ = assert(random_list 5 <> random_list 5);; 

(* Exo 29 *)

type abin =
  | Leaf
  | Node of int * abin * abin;;

(* 
  insert :
  Entrée : a un abin et value un entier
  Sortie : un abin
  Spec : Fonction qui insere l'entier dans l'ABR donne en parametre. Pour chaque noeud, on regarde dans
      quel sous arbre ranger la valeur et on fait l'insertion dans ce dernier . Si la valeur est deja dans
      l'arbre, une exception se lève
*)
let rec (insert:abin->int->abin) = fun a -> fun value ->
  match a with
    | Leaf -> Node(value,Leaf,Leaf)
    | Node(i,a1,a2) -> if (i=value) then failwith "Valeur deja présente" else
          if(i<value) then Node(i,a1,(insert a2 value))  else Node(i,(insert a1 value),a2);;

(* Version 2 du trie ABR *)
(* 
listToABR :
Entrée : Une listl et un ABR 
Sortie : Un ABR
Spec : ajoute tous les élèments de la liste dans l'abre
*)
let rec listToABR = fun l a ->
  match l with
  | [] -> a
  | x::s -> listToABR s (insert a x);;

(* 
parcours :
Entrée : Un arbre
Sortie : Une listl
Spec : parcours l'arbre construit de facon infixe en ajoutant les éléments a une liste d'entiers
*)
let rec parcours = fun a->
  match a with
  | Leaf -> []
  | Node(x,g,d) -> (parcours g) @ [x] @ (parcours d);;

(* 
triABR :
Entrée : Une listl 
Sortie : Une listl
Spec : Une fonction qui prend une liste de valeur et la met dans l'ordre croissant grace a un ABR
*)
let triABRV2 = fun l -> parcours (listToABR l Leaf);;



(*  EXO  26  *)
exception ListeVide;;
(*Partie A*)
let rec trouver_min_i_rec = fun l m c ->
  match l with
  | [] -> (m,c)
  | x :: s -> if (x<m) then trouver_min_i_rec s x (m::c)
                    else trouver_min_i_rec s m (x :: c);; 

let (trouve_min_i:int list -> int*int list) = fun l ->
  match l with
  | [] -> raise ListeVide
  | x :: s -> trouver_min_i_rec s x [];;

let (a,_) = trouve_min_i [1;2;7;0;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_i [1;2;7;10;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_i [1;2;7;10;82;7;9] in assert (a=1);;
let (a,_) = trouve_min_i [10;2;7;12;82;7;1] in assert (a=1);;

(*Partie B*)
let rec trouve_min_rec = fun comp l m c ->
  match l with
  | [] -> (m,c)
  | x::s -> if (comp x m) then trouve_min_rec comp s x (m::c)
                          else trouve_min_rec comp s m (x::c);;

let (trouve_min:('a->'a->bool)->'a list -> 'a * 'a list) = fun comp l ->
  match l with
  | [] -> raise ListeVide
  | x :: s -> trouve_min_rec comp s x [];;

let (a,_) = trouve_min (fun a b->a>b) [1;2;7;0;82;7;0] in assert (a=82);;
let (a,_) = trouve_min (fun a b->a<b) [1;2;7;0;82;7;0] in assert (a=0);;
let (a,_) = trouve_min (fun a b->a<b) [2;2;2;2;2;2;2] in assert (a=2);;

(*Partie C*)
let trouve_min_iV2 = fun l -> trouve_min (fun a b->a<b) l;;
(* Equivalent a : let trouve_min_iV2 = trouve_min (fun a b->a<b);;*)
let (a,_) = trouve_min_iV2 [1;2;7;0;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_iV2 [1;2;7;10;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_iV2 [1;2;7;10;82;7;9] in assert (a=1);;
let (a,_) = trouve_min_iV2 [10;2;7;12;82;7;1] in assert (a=1);;

(*Partie D : facultative*)
type typeObjet =
  | LecteurMP3
  | AppareilPhoto
  | Camera
  | Telephone
  | Ordinateur;;

type marque = 
  | Alpel
  | Syno
  | Massung
  | Liphisp;;

type article = typeObjet * marque * int * int;;
let (trouve_min_a:article list->article * article list) = 
  trouve_min (fun (t1,m1,p1,n1) (t2,m2,p2,n2) -> p1 < p2);;

trouve_min_a [(Ordinateur,Alpel,1000,10);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,10)];;


(*Exo 27*)
let rec (tri_selection:('a -> 'a -> bool) -> 'a list -> 'a list) =fun comp l->
  match l with
  | [] -> []
  | x::s -> let (a,b) = (trouve_min comp (x::s)) in [a]@(tri_selection comp b);;



let listTest = [5;2;8;6;3;1;4];;

let deb = Sys.time() in
let _ = triABRV2 listTest
in Sys.time() -. deb;;


let deb2 = Sys.time() in
let _ = tri_selection (fun a b->a<b) listTest
in Sys.time() -. deb2;;


(* Exo 30 *)

let rec (renv:'a list->'a list) = fun l ->
match l with
  | [] -> []
  | x::s -> renv s @ [x];;

(* Exo 31 *)


let rec renv_app = fun l1 l2 ->
match l1 with
  | [] -> l2
  | x::s -> renv_app s (x :: l2);;


(* Exo 33 *)

let l1 = random_list 10000;;
let l2 = random_list 10000;;
let l3 = random_list 10000;;


let concat_r = Sys.time() in
let _ = l1 @ (l2 @ l3)
in Sys.time() -. concat_r;;


let concat_l = Sys.time() in
let _ = (l1 @ l2) @ l3
in Sys.time() -. concat_l;;

(* Exo 34 *)

let rec (dromadaire:'a list->('a->'a->bool)->'a) = fun l f ->
match l with
  | [] -> failwith("Liste vide")
  | x::[] -> x
  | x::s -> let x2 = dromadaire s f in if(f x x2) then x else x2;;


let listDeTest = [45;25;4;78;2;12;41;118;20;21];;

let _ = dromadaire listDeTest (fun a b -> a>b);;


let rec chameau = fun l f ->
  match l with
    | [] -> failwith("Liste vide")
    | x::[] -> failwith("Pas possible")
    | x::x2::[] -> if(x<x2) then (x2,x) else (x,x2)
    | x::s -> let (x2,y2) = chameau s f in if(f x x2) then (x,x2) else if(f x y2) then (x2,x)  else (x2,y2);;


type produit =
  | LecteurMP3
  | AppareilPhoto
  | Camera
  | Telephone
  | Ordinateur;;

type marque = 
  | Alpel
  | Syno
  | Massung
  | Liphisp;;

type prix = int;;

type stock = int;;

type article = produit * marque * prix * stock;;

(* Exo 37 *)

let isIt = fun p m prix p1 m1 prix1 -> p=p1 && m=m1 && prix = prix1;; 


let rec est_en_stock = fun p m prix l ->
match l with
| [] -> false
| x::s -> let (p1,m1,prix1,stock) = x in
          if(isIt p m prix p1 m1 prix1) then stock <> 0 else est_en_stock p m prix s;;

(* Exo 38 *)

let rec ajoute_article = fun (p,m,pr,st) l ->
match l with
  | [] -> [(p,m,pr,st)]
  | x::s -> let (p1,m1,pr1,st1) = x in
            if(isIt p m pr p1 m1 pr1)
            then (p1,m1,pr1,st1+st) :: s 
            else x :: ajoute_article (p,m,pr,st) s;;

(* Exo 39 *)

let rec enleve_article = fun l (p,m,pr,st) ->
match l with
  | [] -> []
  | (p1,m1,pr1,st1)::s -> if(isIt p m pr p1 m1 pr1) then s else (p1,m1,pr1,st1) :: (enleve_article s (p,m,pr,st));;


(*  REPRENDRE  *)

(* Exo 40*) 


let rec ces_produit = fun p l ->
match l with
  | [] -> []
  | (p1,m1,pr1,st1) :: s -> if(p1 = p)
                            then (p1,m1,pr1,st1) :: ces_produit p l
                            else ces_produit p s;;


(* Exo 41 *)

let deuxieme_moins_cher = fun p l -> chameau (ces_produit p l) (fun (p,m,pr,st)(p1,m1,pr1,st1) -> pr < pr1);;

(* Exo 43  *)

let rec achete = fun (p,m,pr,st) l ->
match l with
  | [] -> failwith("Liste vide")
  | (p1,m1,pr1,st1):: s -> if(isIt p m pr p1 m1 pr1)
                           then (p1,m1,pr1,st1-1)::s
                           else (p1,m1,pr1,st1) :: achete (p,m,pr,st) s;;

let rec commande = fun l ->
match l with
  | [] -> []
  | (p,m,pr,st) :: s -> if(st = 0)
                        then (p,m,pr,st) :: commande s
                        else commande s;;
