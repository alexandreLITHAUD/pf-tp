(* ====================== TP 3 - Manipulation de Listes ====================== *)

(* >  Samuel Brun ; Alexandre Lithaud  *)


(* ====================== Echauffement sur les listes (exo 16) ====================== *)

type listent =
  | Nil
  | Cons of int * listent;;

let rec longueurL = fun l ->
  match l with
  | Nil -> 0
  | Cons(x, s) -> 1 + longueurL s;;

let rec sommeL = fun l ->
  match l with
  | Nil -> 0
  | Cons (x,s) -> x + sommeL s;;

let rec positifL = fun l ->
  match l with
  | Nil -> true
  | Cons (x,s) -> (x>0)&&(positifL s);;

let rec ajoutFinL = fun l e ->
  match l with
  | Nil -> Cons (e,Nil)
  | Cons (x,s) -> Cons (x, ajoutFinL s e);;

let rec concatL = fun l1 l2 ->
  match l1 with
  | Nil -> l2
  | Cons (x,s) -> Cons (x, concatL s l2);;


(* ====================== 3.1 Tri par sélection ====================== *)


(* Exo 26 *)

exception ListeVide;;


(*Partie A*)

let rec (trouver_min_i_rec:'a list->'a->'a list->'a*'a list) = fun l m c ->
  match l with
  | [] -> (m,c)
  | x :: s -> if (x<m) then trouver_min_i_rec s x (m::c)
                    else trouver_min_i_rec s m (x :: c);;

let (trouve_min_i:int list -> int*int list) = fun l ->
  match l with
  | [] -> raise ListeVide
  | x :: s -> trouver_min_i_rec s x [];;

let (a,_) = trouve_min_i [1;2;7;0;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_i [1;2;7;10;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_i [1;2;7;10;82;7;9] in assert (a=1);;
let (a,_) = trouve_min_i [10;2;7;12;82;7;1] in assert (a=1);;


(* Partie B *)

(*
trouve_min_rec :
Entrée : Un predicat comp sur 'a, une liste l 'a, un 'a représentant le max vu jusqu'ici, et c la liste 'a sans le max courant
Sortie : Un couple d'un element de 'a et d'une liste de 'a
Spec : si un nouveau min est trouvé, on ajoute l'ancien a la liste et on chercher avec se nouveau min, sinon on garde l'ancien min et on ajoute l'élément 
  a notre liste 
*)
let rec trouve_min_rec = fun comp l m c ->
  match l with
  | [] -> (m,c)
  | x::s -> if (comp x m) then trouve_min_rec comp s x (m::c)
                          else trouve_min_rec comp s m (x::c);;

(*
trouve_min :
Entrée : Un predicat sur 'a, une liste 'a
Sortie : Un couple d'un element de 'a et d'une liste de 'a
Spec : Fonction qui trouve le minimun de la liste en fonction du prédicat d'entrée
*)
let (trouve_min:('a->'a->bool)->('a list) -> 'a * ('a list)) = fun comp l ->
  match l with
  | [] -> raise ListeVide
  | x :: s -> trouve_min_rec comp s x [];;

let (a,_) = trouve_min (fun a b->a>b) [1;2;7;0;82;7;0] in assert (a=82);;
let (a,_) = trouve_min (fun a b->a<b) [1;2;7;0;82;7;0] in assert (a=0);;
let (a,_) = trouve_min (fun a b->a<b) [2;2;2;2;2;2;2] in assert (a=2);;


(* Partie C *)

(*
trouve_min_iV2 :
Entrée : une liste 'a
Sortie : Un couple d'un element de 'a et d'une liste de 'a
Spec : Fonction qui trouve le minimun de la liste en utilisant le trouve_min définit ci-dessus (pour cela, il faut passer la fonction prédicat qui etant 
  donnée 2 entiers (ou float), rend vrai si le premier est inférieur au 2eme)
*)
let trouve_min_iV2 = fun l -> trouve_min (fun a b->a<b) l;;
(* Equivalent a : let trouve_min_iV2 = trouve_min (fun a b->a<b);;*)
let (a,_) = trouve_min_iV2 [1;2;7;0;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_iV2 [1;2;7;10;82;7;0] in assert (a=0);;
let (a,_) = trouve_min_iV2 [1;2;7;10;82;7;9] in assert (a=1);;
let (a,_) = trouve_min_iV2 [10;2;7;12;82;7;1] in assert (a=1);;


(* Partie D : facultative *)

type typeObjet =
  | LecteurMP3
  | AppareilPhoto
  | Camera
  | Telephone
  | Ordinateur;;

type marque =
  | Alpel
  | Syno
  | Massung
  | Liphisp;;

type article = typeObjet * marque * int * int;;

(*
trouve_min_a :
Entrée : une liste d'articles
Sortie : Un couple d'un article et d'une liste d'articles
Spec : recherche l'article de poids minimal parmis la liste des articles. Pour cela on doit pouvoir comparer des prix d'article, il faut donc récuperer la
  3 eme composante des tuples d'articles et la comparer
*)
let (trouve_min_a:article list->article * article list) =
  trouve_min (fun (t1,m1,p1,n1) (t2,m2,p2,n2) -> p1 < p2);;

trouve_min_a [(Ordinateur,Alpel,1000,10);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,10)];;
trouve_min_a [(Ordinateur,Alpel,10,10)];;
trouve_min_a [(Ordinateur,Alpel,1000,10);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,10);(Telephone,Massung,480,10)];;


(* Exo 27*)

(*
(tri_selection :
Entrée : Un predicat sur 'a, une liste 'a
Sortie : une liste de 'a
Spec : Tri la liste de 'a selon le prédicat donné en entrée en utilisant la recherche du min ci-dessus
*)
let rec (tri_selection:('a -> 'a -> bool) -> 'a list -> 'a list) = fun comp l->
  match l with
  | [] -> []
  | x::s -> let (a,b) = (trouve_min comp (x::s)) in a::(tri_selection comp b);;

tri_selection (fun a b->a<b) [];;
tri_selection (fun a b->a<b) [3;9;0;1;2;34;7;10];;
tri_selection (fun a b->a<b) [1;2;3;4];;
tri_selection (fun a b->a<b) [4;3;2;1];;
tri_selection (fun a b->a<b) [2;2;1;1];;

(*
(tri_selection_i :
Entrée : une liste 'a
Sortie : une liste de 'a
Spec : Tri la liste selon le prédicat (a<b) en utilisant le tri_selection
*)
let tri_selection_i = tri_selection (fun a b->a<b);;
tri_selection_i [];;
tri_selection_i [3;9;0;1;2;34;7;10];;
tri_selection_i [1;2;3;4];;
tri_selection_i [4;3;2;1];;
tri_selection_i [2;2;1;1];;

let (tri_selection_a:article list->article list) = tri_selection (fun (t1,m1,p1,n1) (t2,m2,p2,n2) -> p1 < p2);;

tri_selection_a [(Ordinateur,Alpel,1000,10);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,10)];;



(* ====================== 3.2 Mesure de performances ====================== *)


(* Exo 28 *)

let max_int_alea = 1000000;;

(*
liste_alea :
Entrée : un entier
Sortie : une liste d'entier
Spec : Genere une liste d'entier aleatoire de taille donnée en entrée (il peut y avoir des doublons)
*)
let rec (liste_alea:int->(int list)) = fun n ->
  if n = 0 then [] else (Random.int max_int_alea) ::(liste_alea (n-1));;

(* genere une liste de valeurs aléatoire sans doublons *)
(* recherche si un élément appartient a la liste *)
let rec (isIn:int->int list->bool) = fun i l ->
  match l with
    | [] -> false
    | x::s -> if(x=i) then true else isIn i s;;

let rec (list_alea_diff_rec:int->int list->(int list)) = fun n l->
  if n = 0 then [] else let x = Random.int max_int_alea in if(isIn x l) then list_alea_diff_rec n l else x :: list_alea_diff_rec(n-1)(x::l);;

(*
list_alea_diff :
Entrée : un entier
Sortie : une liste d'entier
Spec : Genere une liste d'entier aleatoire tous différent de taille donnée en entrée
*)
let (list_alea_diff:int->(int list)) = fun n ->
  if n = 0 then [] else list_alea_diff_rec n [];;

Random.self_init;;
liste_alea 5;;
liste_alea 9;;
liste_alea 0;;
liste_alea 1;;

list_alea_diff 100;;
list_alea_diff 1;;
list_alea_diff 0;;


(* Exo 29 *)

(* Fonctions reprises du TP précédent : *)

type abin =
  | Leaf
  | Node of int * abin * abin;;

let rec (insert:abin->int->abin) = fun a -> fun value ->
  match a with
    | Leaf -> Node(value,Leaf,Leaf)
    | Node(i,a1,a2) -> if (i=value) then failwith "Valeur deja présente" else
          if(i<value) then Node(i,a1,(insert a2 value))  else Node(i,(insert a1 value),a2);;

let rec listToABR = fun l a ->
  match l with
  | [] -> a
  | x::s -> listToABR s (insert a x);;

let rec parcours = fun a->
  match a with
  | Leaf -> []
  | Node(x,g,d) -> (parcours g) @ [x] @ (parcours d);;

let triABRV2 = fun l -> parcours (listToABR l Leaf);;



(* === FONCTION DE GENERATION === *)

let rec genereTrierInverse = fun n->
  if n = 0 then [] else n::(genereTrierInverse (n-1));;

let _ = genereTrierInverse 10;;
let _ = genereTrierInverse 1;;


let rec genereTrier = fun n i->
  if n = i then [] else (i+1)::(genereTrier n (i+1));;

let _ = genereTrier 10 0;;
let _ = genereTrier 1 0;;


(* Calcul de performance des différences fonction de tri de liste  *)

let lalea = list_alea_diff 10000;;

let time_tri_ABR = Sys.time() in
  let _ = triABRV2 lalea
    in Sys.time() -. time_tri_ABR;;
let time_tri_selection = Sys.time() in
  let _ = tri_selection_i lalea
    in Sys.time() -. time_tri_selection;;
(* Sur une tres grande liste, on voit clairement la différence de temps entre l'execution de nos deux fonctions,
   Le tri via ABR s'execute en moins de 0.01 seconde alors que le tri par insertion prends plus de 3 secondes
  Ces résultats sont cohérent avec la complexité des programmes. En effet, le tri par selection est en moyenne de O(n^2) 
    alors que le tri par l'arbre est en moyenne de O(n log n). 
   Il est donc naturel que la selection soit bien plus lente que le tri par arbre
   *)

(* Testons sur un cas particulier : liste trier a l'inverse *)
let lalea = genereTrierInverse 10000;;
let time_tri_ABR = Sys.time() in
  let _ = triABRV2 lalea
    in Sys.time() -. time_tri_ABR;;
let time_tri_selection = Sys.time() in
  let _ = tri_selection_i lalea
    in Sys.time() -. time_tri_selection;;
(* Ce cas correspond au pire cas des 2 programmes ci-dessus. Cette fois ci, la différence est clairement moins évidente, en effet les 2 programmes sont dans
   le pire cas en compléxité de O(n^2) *)

(* Testons sur un dernier cas particulier : listes deja triés *)
let lalea = genereTrier 10000 0;;
let time_tri_ABR = Sys.time() in
  let _ = triABRV2 lalea
    in Sys.time() -. time_tri_ABR;;
let time_tri_selection = Sys.time() in
  let _ = tri_selection_i lalea
    in Sys.time() -. time_tri_selection;;
(* Ce cas reste en O(n^2) pour les 2 programmes, il montre encore que le tri_selection est moins efficace. 
   Il nous montre aussi que pour avoir la meilleur efficacitée sur le tri abr, il faut que les éléments soit répartis aléatoirement dans le tableau *)


(* ====================== 3.3 Renversement de listes ====================== *)


(* ==== 3.3.1 Deux Programmes ==== *)


(* Exo 30 *)

(*
renv :
Entrée : une liste de 'a
Sortie : une liste de 'a
Spec : Fonction naive qui retourne les element de la liste en entrée
*)
let rec (renv:'a list->'a list) = fun l ->
  match l with
  | [] -> []
  | x :: s -> (renv s) @ [x] ;;

renv [];;
renv [2;4;6;7];;

(* Exo 31 *)

let rec (renv_app:'a list->'a list->'a list) = fun l1 l2 ->
  match l1 with
  | [] -> l2
  | x :: s -> renv_app s (x::l2);;

(*
renvV2 :
Entrée : une liste de 'a
Sortie : une liste de 'a
Spec : Fonction qui retourne les element de la liste en entrée
*)
let (renvV2:'a list->'a list) = fun l -> renv_app l [];;

renvV2 [];;
renvV2 [2;4;6;7];;


(* ==== 3.3.2 Mesure de performances ==== *)

(* Exo 32 *)
(* Calcul de performance des différences fonction de retournement de liste  *)

let l1 = liste_alea 10000;;

let deb = Sys.time() in
  let _ = renv l1
    in Sys.time() -. deb;;

let deb = Sys.time() in
  let _ = renvV2 l1
    in Sys.time() -. deb;;

(*
   Explication différence de temps :
   renv classique : O(n^2)
    Ici le renversement parcours la liste une premiere fois et pour chaque élément de cette liste fait une concaténation. La concaténation parcours elle
    aussi une fois la premiere des 2 liste qui lui est passé en argument. Ainsi donc la complexité est en O(n^2)
    
   renv opti : O(n)
   Le couts de l'ajout en tete d'une liste étant constant, un seul parcours de notre liste est nécéssaire, d'ou la grande éfficacité de ce programme 
   comparativement a celui naif
*)


(* Exo 33 *)

let l1 = liste_alea 10000;;
let l2 = liste_alea 10000;;
let l3 = liste_alea 10000;;

(* Ici, on veut comparer l'efficacité de l'opérateur @ en fonction de son associativitée.
   On sait que la concaténation parcours entierement la premiere liste et la concatene avec la 2 eme. On peut donc imaginer que plus la premiere
   liste est longue, plus la durée necessaire pour la parcourir sera longue. *)
let concat_r = Sys.time() in
let _ = l1 @ (l2 @ l3)
in Sys.time() -. concat_r;;
(* Complexité :
   ici, le logiciel va dans un premier temps effectuer l2@l3 :
    il va donc parcourir les 10 000 elements de l2 et les ajouter a l3. 
   Le programme va ensuite concatener l1 a l4 (=def l2@l3)
    il va donc parcourir les 10 000 éléments de l1 et les ajouter a L4
  -> il parcours donc 20 000 éléments *)


let concat_l = Sys.time() in
let _ = (l1 @ l2) @ l3
in Sys.time() -. concat_l;;
(* Complexité :
  ici, le logiciel va dans un premier temps effectuer l1@l1 :
    il va donc parcourir les 10 000 elements de l1 et les ajouter a l2. 
  Le programme va ensuite concatener l4 a l3 (=def l1@l2)
    il va donc parcourir les 20 000 éléments de l4 (10 000 de l1 puis 10 000 de l2) et les ajouter a L4 
  -> il parcours donc 30 000 éléments, soit 10 000 de plus que la concat a droite
  Ce qui explique la différence entre les temps d'éxecution *)   

(* ====================== 3.4 Chameau et dromadaire ====================== *)


(* Exo 34 *)

(*
dromadaire :
Entrée : Un prédicat de comparaison sur 'a, une liste de 'a
Sortie : un element de 'a
Spec : Donne l'élément remplissant le mieux le predicat de comparaison
*)
let rec (dromadaire:('a->'a->bool)->('a list)->'a) = fun comp l ->
  match l with
  | [] -> raise ListeVide
  | x :: [] -> x
  | x :: s -> let a = dromadaire comp s in (if comp x a then x else a);;

assert(dromadaire (fun a b->a>b) [1;2;4;0;8;19]=19);;
assert(dromadaire (fun a b->a<b) [1;2;4;0;8;19]=0);;
assert(dromadaire (fun a b->true) [1;2;4;0;8;19]=1);;
assert(dromadaire (fun a b->false) [1;2;4;0;8;19]=19);;


(* Exo 35 *)

(*
chameau :
Entrée : Un prédicat de comparaison sur 'a, une liste de 'a
Sortie : un couple de 'a
Spec : Donne les deux élément remplissant le mieux le predicat de comparaison
*)
let rec (chameau:('a->'a->bool)->('a list)->'a * 'a) = fun comp l ->
  match l with
  | [] | _ :: [] -> raise ListeVide (* Impossible de trouver 2 elements qui respectent *)
  | x1 :: x2 :: [] -> if comp x1 x2 then (x1,x2) else (x2,x1) (* Les 2 éléments sont forcement les 2 derniers *)
  | x :: s -> let (a,b)=chameau comp s in (* Cas général, on a le couple vérifiant la cond et on regarde pour *)
              if(comp x a) then (x,a) else (* l'element x, soit on l'ajout dans le couple, soit non *)
                (if (comp x b) then (a,x) else (a,b));;

assert(chameau (fun a b->a>b) [1;2;4;0;8;19]=(19,8));;
assert(chameau (fun a b->a<b) [1;2;4;0;8;19]=(0,1));;
assert(chameau (fun a b->a<b) [1;2]=(1,2));;

(* ====================== 3.5 Le magasin (facultatif) ====================== *)


(* Exo 36 *)

type produit =
  | LecteurMP3
  | AppareilPhoto
  | Camera
  | Telephone
  | Ordinateur;;

type marque =
  | Alpel
  | Syno
  | Massung
  | Liphisp;;

type prix = int;;

type stock = int;;

type article = produit * marque * prix * stock;;

let s1 = [(Ordinateur,Alpel,1000,10);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,1);(Telephone,Massung,100,0)]

(* Exo 37 *)


(*
memeProduit :
Entrée : Un produit, une marque, un prix, un produit, une marque, un prix
Sortie : Un boolean
Spec : Indique si les deux article sont identique ou non
*)
let (memeProduit:produit->marque->prix->produit->marque->prix->bool) = fun t1 m1 p1 t2 m2 p2 ->
  (t1 = t2) && (m1 = m2) && (p1 = p2);;

(*
est_en_stock :
Entrée : Un produit, une marque, un prix, une liste d'article
Sortie : Un boolean
Spec : Indique si produit voulu est en stock ou non
*)
let rec (est_en_stock:produit->marque->prix->(article list)->bool) = fun t m p l ->
  match l with
  | [] -> false
  | x :: s -> let (pro,mar,pri,sto) = x in
          (if memeProduit t m p pro mar pri then sto>0 else (est_en_stock t m p s));;

assert (est_en_stock Telephone Massung 430 s1 = true);;
assert (est_en_stock Telephone Massung 100 s1 = false);;
assert (est_en_stock Telephone Alpel 430 s1 = false);;

(* Exo 38 *)

(*
ajoute_article :
Entrée : Un article, une liste d'article
Sortie : Une liste d'article
Spec : Ajoute a la liste l'article en entrée l'article
*)
let rec (ajoute_article:article->(article list)->(article list)) = fun (t,m,p,s) l ->
  match l with
  | [] -> [(t,m,p,s)]
  | (t',m',p',s')::suite -> if( memeProduit t m p t' m' p' )
            then (t',m',p',s'+s)::suite
            else (t',m',p',s')::(ajoute_article (t,m,p,s) suite);;

ajoute_article (Telephone,Massung,430,1) s1;;
ajoute_article (Telephone,Massung,100,10) s1;;
ajoute_article (Telephone,Alpel,1500,16) s1;;


(* Exo 39 *)

(*
enleve_article :
Entrée : Une liste d'article, un article
Sortie : Une liste d'article
Spec : Enleve a la liste l'article en entrée l'article si il existe
*)
let rec (enleve_article:article list->article->(article list)) = fun l (t,m,p,s) ->
  match l with
  | [] -> []
  | (t',m',p',s') :: suite -> if( memeProduit t m p t' m' p')
            then suite else (t',m',p',s')::(enleve_article suite (t,m,p,s));;

enleve_article s1 (Telephone,Massung,430,1);;
enleve_article s1 (Telephone,Massung,100,10);;
enleve_article s1 (Telephone,Alpel,1500,16);;


(* ==== Aider un client ==== *)

(* Exo 40 *)

(*
ces_pruduits :
Entrée : Un produit, une liste d'article
Sortie : Une liste d'article
Spec : Rend les articles qui convienne avec le produit en entrée
*)
let rec (ces_produits:produit->(article list)->(article list)) = fun prod l ->
  match l with
  | [] -> []
  | (t,m,p,s) :: suite -> if(t = prod) then (t,m,p,s)::(ces_produits prod suite)
          else (ces_produits prod suite);;

let s2 = [(Ordinateur,Alpel,1000,10);(Telephone,Alpel,430,1);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,1);(Telephone,Massung,100,0)];;

ces_produits Telephone s2;;
ces_produits Ordinateur s1;;
ces_produits LecteurMP3 s1;;


(* Exo 41 *)

(* Version minimaliste utilsant la fonction chameau*)
let lePlusCourantV1 = fun p l -> chameau (fun (t1,m1,p1,s1) (t2,m2,p2,s2) -> p1<p2) (ces_produits p l);;



let rec (lesPlusCourantProd2:produit->article list->article->article->article) = fun p l (t1,m1,p1,s1) (t2,m2,p2,s2) ->
  match l with
  | [] -> (t2,m2,p2,s2)
  | (t,m,pr,s) :: suite -> if(p=t)then
                                (if (pr<p1)
                                    then lesPlusCourantProd2 p suite (t,m,pr,s) (t1,m1,p1,s1)
                                else
                                    (if (pr<p2)
                                        then lesPlusCourantProd2 p suite (t1,m1,p1,s1) (t,m,pr,s)
                                    else lesPlusCourantProd2 p suite (t1,m1,p1,s1) (t2,m2,p2,s2))
                                )
                            else (lesPlusCourantProd2 p suite (t1,m1,p1,s1) (t2,m2,p2,s2));;

let rec (lesPlusCourantProd1:produit->article list->article->article) = fun p l (t1,m1,p1,s1) ->
  match l with
  | [] -> failwith(" Un seul article de type produit ")
  | (t,m,pr,s) :: suite -> if(p=t)then (if(pr<p1) then lesPlusCourantProd2 p suite (t,m,pr,s) (t1,m1,p1,s1) else lesPlusCourantProd2 p suite (t1,m1,p1,s1) (t,m,pr,s))
            else lesPlusCourantProd1 p suite (t1,m1,p1,s1);;


(* Version efficace avec un seul parcours *)

(*
lesPlusCourant :
Entrée : Un produit, une liste d'articles
Sortie : Un article
Spec : Retourne l'article ayant le meme type de produit mais avec le 2ème prix le moins cher
*)
let rec (lesPlusCourant:produit->article list->article) = fun p l ->
  match l with
  | [] -> failwith(" Aucun article de type produit ")
  | (t,m,pr,s) :: suite -> if(p=t)then lesPlusCourantProd1 p suite (t,m,pr,s)
            else lesPlusCourant p suite;;

let s2 = [(Ordinateur,Alpel,1000,10);(Telephone,Alpel,430,1);(AppareilPhoto,Syno,800,25);(Telephone,Massung,430,1);(Telephone,Massung,100,0)];;

lesPlusCourant Telephone s2;;

(* Exo 42 *)

(*
budget :
Entrée : Deux entier, une liste d'articles
Sortie : Une liste d'article
Spec : Donne la liste des produits achetable dans la fourchette de budget donne en entrée
*)
let rec (budget:int->int->article list->article list) = fun min max l ->
  match l with
  | [] -> []
  | (t,m,p,s) :: suite -> if( p >= min && p <= max)then (t,m,p,s)::budget min max suite
        else budget min max suite;;


budget 200 1500 s2;;
budget 50 200 s2;;
budget 0 80 s2;;
budget 1000 2500 s2;;

(* ==== Gestion des stocks ==== *)

(* Exo 43 *)

(*
achete :
Entrée : Une liste d'article, un produit, une marque, un prix
Sortie : Une liste d'article
Spec : Fait diminuer de 1 le stock du produit donné en entrée
*)
let rec (achete:article list->produit->marque->prix->article list) = fun l t m p ->
  match l with
  | [] -> []
  | (t1,m1,p1,s1) :: suite -> if(memeProduit t m p t1 m1 p1)
        then (t1,m1,p1,max (s1-1) 0)::suite
        else (t1,m1,p1,s1)::(achete suite t m p);;


(* Exo 44 *)

(*
commande :
Entrée : Une liste d'article
Sortie : Une liste d'article
Spec : Retourne la liste des articles a commander (qui n'ont plus de stock)
*)
let rec (commande:article list->article list)= fun l ->
  match l with
  | [] -> []
  | (t1,m1,p1,s1) :: suite -> if(s1 <= 0)then (t1,m1,p1,s1)::(commande suite)
                              else commande suite;;


commande s2;;


(*===================================== Fin =====================================*)
