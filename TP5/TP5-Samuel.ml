#load "qtparser.cmo";;
open Qtparser 

(* QUADTREE *)
(*
type quadtree = 
  | F of int 
  | N of quadtree * quadtree * quadtree * quadtree;;
*)

(* 5.1 *)
(*
n n n n   n n n n
n n n n   n n n n
n n b b   b b b b
n n b b   b b b b

n n n n   b b b b
n n n n   b b b b
n n n n   b b b b
n n n n   b b b b

G 8*8
G 4*4
N 2*2
N 2*2
B 2*2
N 2*2
G 4*4 
N 2*2
N 2*2
B 2*2
B 2*2
N 4*4
B 4*4
*)
let q = N(N(F 0,F 0,F 1,F 0),N(F 0,F 0,F 1,F 1),F 1,F 0);;


