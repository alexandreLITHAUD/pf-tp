
let f1 = fun x -> fun y -> fun z -> x*y*z;;

let f2 = fun x -> fun y -> fun z -> x+y+z;;

let x = f1(2)(3)(4);;
let y = f2(2)(3)(4);;

