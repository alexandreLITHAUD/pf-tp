
type couleur = | Red
               | Green
               | Blue
               | Yellow;;


type numero = | Zero
              | Un
              | Deux
              | Trois
              | Quatre
              | Cinq
              | Six
              | Sept
              | Huit
              | Neuf
              | Plus_Deux
              | Interdiction
              | Inversion;;

type carte = | Classique of couleur * numero
             | Joker
             | Super_Joker;;

