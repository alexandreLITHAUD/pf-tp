type semaine = | Lundi of string
               | Mardi of string
               | Mercredi of string
               | Jeudi of string
               | Vendredi of string
               | Samedi of string
               | Dimanche of string;;


type point2D = int * int;;

let origine:point2D = (0,0);;

type segment = point2D * point2D;;

type figure = | Carre of point2D * point2D * point2D * point2D
              | Rectangle of point2D * point2D * point2D * point2D
              | Cercle of point2D * segment;;

let carre:figure = Carre((1,1),(2,2),(3,3),(4,4));;

let rect:figure = Rectangle((1,1),(1,1),(1,1),(1,1));;

let cercle:figure = Cercle((1,1),((1,1),(1,1)));;
