
let (cube:float->float) = fun a -> a*.a*.a;;

let x = cube(2.);;

let (isPositif:int->bool) = fun a -> if(a>=0) then true else false;;

let y = isPositif(-1);;
let b = isPositif(1);;

let (isPair:int->bool) = fun a -> if((a mod 2)=0) then true else false;;

let c = isPair(2);;
let d = isPair(3);;

let (signe:int->int) = fun a -> if(a=0) then 0 else if(a>0) then 1 else -1;;

let e = signe(0);;
let f = signe(5);;
let g = signe(-9);;

