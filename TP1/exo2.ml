let pi_sur_4 = 3.14 /. 4.;;
let dans_l_ordre = 1<2;;
let positif = let a = 42 in if a >= 0 then true else false;;
let double absolu = let x = -.2.7 in (if (x < 0.) then x else -.x) *. 2.;;
