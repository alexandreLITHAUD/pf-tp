
(*Exo 1*)

let x= 5;;

let r = let x = 7 in 6 * x;; (*42*)
let a = (r-6)/6-6;; (*0*)
let o = r*r-x*x-51;; (*Erreur*) (*1668*)
let u = let x = 9 in if(x<9) then 9 / (x-x) else (x+x) /9;; (*2*)

(*Exo 2*)

let pi_sur_4 = 3.14 /. 4.;;
let dans_l_ordre = 1<2;;
let positif = let a = 42 in if a >= 0 then true else false;;
let double absolu = let x = -.2.7 in (if (x < 0.) then x else -.x) *. 2.;;

(*Exo 3*)

type semaine = | Lundi
               | Mardi
               | Mercredi
               | Jeudi
               | Vendredi
               | Samedi
               | Dimanche;;


type point2D = int * int;;

let origine:point2D = (0,0);;

type segment = point2D * point2D;;

type figure = | Carre of point2D * point2D * point2D * point2D
              | Rectangle of point2D * point2D * point2D * point2D
              | Cercle of point2D * segment;;

let carre:figure = Carre((1,1),(2,2),(3,3),(4,4));;

let rect:figure = Rectangle((1,1),(1,1),(1,1),(1,1));;

let cercle:figure = Cercle((1,1),((1,1),(1,1)));;

(*Exo 4*)

type couleur = | Red
               | Green
               | Blue
               | Yellow;;


type numero = | Zero
              | Un
              | Deux
              | Trois
              | Quatre
              | Cinq
              | Six
              | Sept
              | Huit
              | Neuf
              | Plus_Deux
              | Interdiction
              | Inversion;;

type carte = | Classique of couleur * numero
             | Joker
             | Super_Joker;;

(*Exo 5*)

let (cube:float->float) = fun a -> a*.a*.a;;

let x = cube(2.);;

let (isPositif:int->bool) = fun a -> if(a>=0) then true else false;;

let y = isPositif(-1);;
let b = isPositif(1);;

let (isPair:int->bool) = fun a -> if((a mod 2)=0) then true else false;;

let c = isPair(2);;
let d = isPair(3);;

let (signe:int->int) = fun a -> if(a=0) then 0 else if(a>0) then 1 else -1;;

let e = signe(0);;
let f = signe(5);;
let g = signe(-9);;

(*Exo 6*)

let f1 = fun x -> fun y -> fun z -> x*y*z;;

let f2 = fun x -> fun y -> fun z -> x+y+z;;

let x = f1(2)(3)(4);;
let y = f2(2)(3)(4);;

(*Exo 7*)

let (pyta:int->int->int->bool) = fun a -> fun b -> fun c -> if((a*a+b*b)=(c*c)) then true else false;;

pyta(3)(4)(5);;

pyta(4)(1)(1);;

let (memeSigne:int->int->bool) = fun a -> fun b -> if((a>=0 && b >=0) || a<0 && b<0) then true else false;;

memeSigne(1)(-1);;

memeSigne(-1)(1);;

memeSigne(1)(1);;

memeSigne(-1)(-1);;

(*Exo 8*)

(*Une fonction ayant un int->int->int est une fonction avec 2 argument entier et qui retourne un entier*)

(*Une fonction avant un int->int->bool est une fonction avec 2 arguement entier et qui retourner un boolean*)

(*Exo 9*)

let (min2entiers:int->int->int) = fun a -> fun b -> if(a<b) then a else b;;

let aa = min2entiers(2)(4);;

let bb = min2entiers(6)(2);;

let cc = min2entiers(2)(2);;

(*Exo 10*)

let (min3entier:int->int->int->int) = fun a -> fun b -> fun c -> if(a<b) then if(a<c) then a else a else if(b<c) then b else c;;

min3entier(1)(2)(3);;

min3entier(3)(2)(1);;

min3entier(2)(1)(3);;

(*Exo 11*)

(*let (millieuSeg:segment->point2D) = fun ((x1,x2)(y1,y2)) -> (x1+x2)/2,(y1+y2)/2;;*)

(*Exo 12*)

let (isWeekend:semaine->bool) = fun a -> match a with | Samedi | Dimanche -> true
                                                      | _ -> false;;


isWeekend Lundi;;

(*Exo 13*)

type figures = | Carre of int
               | Rect of int*int
               | Cercle of int;;

let (aire:figures->int) = fun a -> (match a with | Carre a -> a*a | Rect (a,b) -> a*b | Cercle a -> a*3);;

aire(Carre(2));;
aire(Rect(2,3));;
aire(Cercle(3));;

(*Exo 14*)

let valeurCarte = fun a -> match a with | Joker -> 30
                                        | Super_Joker -> 50
                                        | Classique(a,b) ->
                                           match b with
                                           | Plus_Deux | Interdiction | Inversion  -> 20
                                           | Zero -> 0
                                           | Un -> 1
                                           | Deux -> 2
                                           | Trois -> 3
                                           | Quatre -> 4
                                           | Cinq -> 5
                                           | Six -> 6
                                           | Sept -> 7
                                           | Huit -> 8
                                           | Neuf -> 9;;

valeurCarte(Joker);;
valeurCarte(Classique(Red,Un));;

let peuxJouer = fun a -> match a with 
                         | Classique(a,b) -> 
                            (match b with 
                             | Inversion | Interdiction -> true 
                             | _ -> false)
                         | _ -> false;;

peuxJouer(Joker);;
peuxJouer(Classique(Red,Inversion));;

let peuxJouersurAutre = fun a -> fun b ->
                                  match b with
                                    | Joker -> true
                                    | Super_Joker -> true
                                    | Classique(c,n) -> 
                                       match a with
                                       | Joker -> true
                                       | Super_Joker -> true
                                       | Classique(c2,n2) -> c2 = c || n2 = n;;

peuxJouersurAutre(Classique(Red,Un))(Joker);;
peuxJouersurAutre(Classique(Red,Un))(Classique(Red,Six));;
peuxJouersurAutre(Classique(Red,Un))(Classique(Blue,Un));;
 

(*Exo 15*)

type listent = Nil | Cons of int * listent;;

let rec longueur = fun l -> match l with
| Nil -> 0
| Cons(x, s) -> 1 + longueur s;;

let rec somme = fun l -> match l with
| Nil -> 0
| Cons(x,s) -> x + somme(s);;

somme(Cons(5,Cons(2,Nil)));;

let res allPos = fun a -> match a with
| Nil -> true
| Cons(x,s) -> if(x<0) then false else allPos(s);;

(*allPos(Cons(5,Cons(6,Nil)));;*)
(*allPos(Cons(5,Cons((-1),Nil)));;*)

let res addVal = fun a b -> match a with
| Nil -> Cons(b,Nil)
| Cons(x,s) -> Cons(x,addVal s b);;

let res concat = fun a b -> match a with
| Nil -> b
| Cons(x,s) -> Cons(x,concat(s,b));;

(*Exo 16*)

type main = Nil | Carte of carte * main;;

let rec valeurMain = fun a -> match a with
| Nil -> 0
| Carte(x,n) -> valeurCarte(x)+valeurMain(n);;

let rec peuxjouerMain = fun a b c ->
  match a with
    | Nil -> c
    | Carte(ca,s) -> if(peuxJouersurAutre(b)(ca)) then Carte(ca,peuxjouerMain s b c) else peuxjouerMain s b c;;

let res peuxEtreJouerALaSuite = fun a b -> 
  match a with
    | Nil -> true
    | Carte(ca,s) -> if(peuxJouersurAutre b ca) then (peuxEtreJouerALaSuite s ca) else false;;
